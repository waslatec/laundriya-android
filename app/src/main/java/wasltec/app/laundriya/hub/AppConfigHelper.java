package wasltec.app.laundriya.hub;

import android.content.Context;

import wasltec.app.laundriya.R;

public class AppConfigHelper {

    public final static String PENNDING_EN = "PENDING";
    public final static String APPROAVED_EN = "APPROVED";
    public final static String COOKED_EN = "COOKED";
    public final static String UNDER_DELIVERING_EN = "UDER DELIVERING";
    public final static String DELIVERED_EN = "DELIVERED";
    public final static String CANCELLED_EN = "CANCELLED";
    public final static String REJECTED_EN = "REJECTED";
    public final static String DELIVERY_FAILED_EN = "DELIVERY_FAILED";
    public final static String PENDING_TO_DELIVER_EN = "PENDING_TO_DELIVER";

    public final static String PENNDING_AR = "تحت الطلب";
    public final static String APPROAVED_AR = "تم الموافقه";
    public final static String COOKED_AR = "تم طبخه";
    public final static String UNDER_DELIVERING_AR = "فى الطريق";
    public final static String DELIVERED_AR = "تم تسليمه";
    public final static String CANCELLED_AR = "تم رفضه";
    public final static String REJECTED_AR = "تم رفضه";
    public final static String DELIVERY_FAILED_AR = "لم يتم التوصيل";
    public final static String PENDING_TO_DELIVER_AR = "جاهز للتوصيل";

    public final static int PENNDING_STATE = 0;
    public final static int APPROAVED_STATE = 2;
    public final static int COOKED_STATE = 3;
    public final static int UNDER_DELIVERING_STATE = 4;
    public final static int DELIVERED_STATE = 5;
    public final static int CANCELLED_STATE = 6;
    public final static int REJECTED_STATE = 7;
    public final static int DELIVERY_FAILED_STATE = 8;
    public final static int PENDING_TO_DELIVER_STATE = 9;

    public static int CART_ITEM_COUNT = 0;

    public final static String ORDER_ID_EXTRA_KEY = "orderId";

    public static double driver_lat = 0;
    public static double driver_lng = 0;
    public static boolean delivery_flag = false;
    public static int signalR_order_id = -1;

    public static String getOrderState(int state, Context context)
    {
        switch (state)
        {
            case PENNDING_STATE:
              return  context.getString(R.string.app_name);

            case APPROAVED_STATE:
                return context.getString(R.string.app_name);

            case COOKED_STATE:
                return context.getString(R.string.app_name);


            case UNDER_DELIVERING_STATE:
                return context.getString(R.string.app_name);

            case DELIVERED_STATE:
                return context.getString(R.string.app_name);


            case CANCELLED_STATE:
                return context.getString(R.string.app_name);


            case REJECTED_STATE:
                return context.getString(R.string.app_name);

            case DELIVERY_FAILED_STATE:
                return  context.getString(R.string.app_name);

            case PENDING_TO_DELIVER_STATE:
                return  context.getString(R.string.app_name);

            default:
                return  context.getString(R.string.app_name);


        }

    }

    public final static String HUB_HOST = "http://nagdy.service.wasltec.com";
    public final static String HUB_NAme = "NagdyHub";
    public static final String REGISTER_URL = "http://nagdy.service.wasltec.com/api/users/register";
    public static final String CONFIRM_URL = "http://nagdy.service.wasltec.com/api/users/confirm";
    public static final String FORGET_PASSWORD_URL = "http://nagdy.service.wasltec.com/api/users/ForgetPassword";
    public static final String CLIENT_APP_ID = "1";
    public final static String COUNTRY_CODE = "00966";
    public final static int ANDROID_MOBILE_ID = 2;
    public static final String LOGIN_URL = "http://nagdy.service.wasltec.com/token";
    public static final String ADD_DEVICE_URL = "http://nagdy.service.wasltec.com/api/users/AddDevice";
    public static final String SYNC_DATA_URL = "http://nagdy.service.wasltec.com/api/users/SyncData";
    public static final String GOOGLE_PROJ_ID = "99047342426"; ///"527412103588";
    public static final String CATEGORY_ID_KEY = "categoryIdKey";
    public static final String SUBCATEGORY_ID_KEY = "subCategoryIdKey";
    public static final String ADD_ADDRESS_URL = "http://nagdy.service.wasltec.com/api/users/AddAddress";
    public static final String GET_ADDRESSES_URL = "http://nagdy.service.wasltec.com/api/users/AddressesList";
    public static final String GET_MY_ORDERS_URL = "http://nagdy.service.wasltec.com/api/users/OrdersList";
    public static final String GET_BUILDINGS_URL = "http://nagdy.service.wasltec.com/api/users/GetBuldings";
    public static String ITEM_IMAGE_URL = ""; //http://admin.nagdy.wasltec.com/uploads/items/";
    public static String CATEGORY_IMAGE_URL = "";   //http://admin.nagdy.wasltec.com/uploads/Categories/";
    public static final String RATE_SERVICE_URL = "http://nagdy.service.wasltec.com/api/users/Rate";

    public final static String ITEM_ID_EXTRA_KEY = "itemIdKey";

    ////////////payment list///////////////
    public static final int NO_PAYMENT_ID = -1;
    public static final String CHOOSE_PAYMENT_ENG = "choose payment type";
    public static final String CHOOSE_PAYMENT_AR = "اختر نوع الدفع";
    public static final int CASH_ID = 1;
    public static final String CASH_NAME_ENG = "Cash";
    public static final String CASH_NAME_AR = "كاش";
    public static final int PROMOTION_ID = 2;
    public static final String VISA_NAME_ENG = "Visa";
    public static final String VISA_NAME_AR = "فيزا";


}
