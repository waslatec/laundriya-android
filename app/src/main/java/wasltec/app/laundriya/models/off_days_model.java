package wasltec.app.laundriya.models;

/**
 * Created by Amr Heidar on 3/26/2017.
 */

public class off_days_model {

    String Company;
    String FromDate;
    String ToDate;



    public String getCompany() {
        return Company;
    }

    public void setCompany(String company) {
        Company = company;
    }

    public String getFromDate() {
        return FromDate;
    }

    public void setFromDate(String fromDate) {
        FromDate = fromDate;
    }

    public String getToDate() {
        return ToDate;
    }

    public void setToDate(String toDate) {
        ToDate = toDate;
    }
}
