package wasltec.app.laundriya.activities;

import android.content.Context;
import android.os.Bundle;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.Locale;

import wasltec.app.laundriya.DataAccess.ServerCalls;
import wasltec.app.laundriya.R;
import wasltec.app.laundriya.models.StandardWebServiceResponse;
import wasltec.app.laundriya.models.UserModel;
import wasltec.app.laundriya.serverconnection.volley.ConnectionVolley;
import wasltec.app.laundriya.utils.CheckResponse;
import wasltec.app.laundriya.utils.GeneralClass;
import wasltec.app.laundriya.utils.LoginSharedPreferences;

/**
 * Created by raed on 24/04/2017.
 */

public class SplashScr extends AppCompatActivity implements Response.Listener, Response.ErrorListener {
    LoginSharedPreferences loginSharedPreferences;
    Handler handler;
    ServerCalls calls;
    boolean logged = false;
    Context con;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.splash);
        con = SplashScr.this;
        calls = new ServerCalls();
        loginSharedPreferences = new LoginSharedPreferences(this);
        SharedPreferences pref = con.getApplicationContext().getSharedPreferences("language", 0);
        GeneralClass.checkkLanguage(con);

        if (!loginSharedPreferences.getPhoneNumber(this).equals("")) {
            handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    String username = "" + loginSharedPreferences.getPhoneNumber(con);
                    String password = loginSharedPreferences.getPassword(con);
                    ConnectionVolley connectionVolley=null;
                    calls.callServerToLogin(password,username, con, SplashScr.this, SplashScr.this,connectionVolley);
                }
            }, 2000);
        } else {
            handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent intent = new Intent(SplashScr.this, BoardingActivity.class);
                    SplashScr.this.finish();
                    startActivity(intent);
                }
            }, 2000);
        }
    }





    //381415
    @Override
    public void onResponse(Object response) {
        Log.e("response", response.toString());
        try {
            if (CheckResponse.getInstance().checkResponse(this, response.toString(), true)) {
                calls.rememberUser(response.toString(), con);
                finish();
                Intent intent = new Intent(con, HomeActivity.class);
                startActivity(intent);
            } else {
                Intent intent = new Intent(SplashScr.this, BoardingActivity.class);
                SplashScr.this.finish();
                startActivity(intent);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        VolleyLog.d("responseError", "Error: " + error.getMessage());
        try {
            ConnectionVolley.dialog.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}


