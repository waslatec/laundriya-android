package wasltec.app.laundriya.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

import wasltec.app.laundriya.DataAccess.ServerCalls;
import wasltec.app.laundriya.GridSpacingItemDecoration;
import wasltec.app.laundriya.R;
import wasltec.app.laundriya.adapters.CategoriesAdapter;
import wasltec.app.laundriya.adapters.InvoiceAdapter;
import wasltec.app.laundriya.adapters.PricesAdapter;
import wasltec.app.laundriya.models.CategoryModel;
import wasltec.app.laundriya.models.ItemsModel;
import wasltec.app.laundriya.models.OrderModel;
import wasltec.app.laundriya.models.PriceModel;
import wasltec.app.laundriya.models.SyncDataModel;
import wasltec.app.laundriya.serverconnection.volley.ConnectionVolley;
import wasltec.app.laundriya.utils.CheckResponse;
import wasltec.app.laundriya.utils.LoginSharedPreferences;

/**
 * Created by raed on 09/05/2017.
 */

public class Pricing extends Fragment implements Response.Listener, Response.ErrorListener {

    Context con;
    Activity act;
    LoginSharedPreferences loginSharedPreferences;
    ServerCalls calls;
    RecyclerView prices;
    PricesAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_pricing, container, false);

        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onActivityCreated(savedInstanceState);

        act = getActivity();
        con = getActivity();
        loginSharedPreferences = new LoginSharedPreferences(getActivity());
        prices = (RecyclerView) getView().findViewById(R.id.prices);
        calls = new ServerCalls();
        ConnectionVolley connectionVolley = null;
        calls.callServerToSyncData(con, this, this, connectionVolley);


    }

    private List<ItemsModel> decodeJson(String jsonString) {
        try {
            List<ItemsModel> priceitems = new ArrayList<>();
            List<PriceModel> prices = new ArrayList<>();
            List<ItemsModel> items = new ArrayList<>();
            List<ItemsModel> services = new ArrayList<>();
            List<ItemsModel> servicestypes = new ArrayList<>();

            Gson gson = new Gson();
            SyncDataModel syncDataModel = new SyncDataModel();
            gson = new Gson();
            syncDataModel = gson.fromJson(jsonString, SyncDataModel.class);
            gson = new Gson();
            GsonBuilder builder = new GsonBuilder().excludeFieldsWithoutExposeAnnotation();

            prices = gson.fromJson(builder.create().toJson(syncDataModel.getPrices()),
                    new TypeToken<List<PriceModel>>() {
                    }.getType());

            services = gson.fromJson(builder.create().toJson(syncDataModel.getServices()),
                    new TypeToken<List<ItemsModel>>() {
                    }.getType());
            servicestypes = gson.fromJson(builder.create().toJson(syncDataModel.getServicestypes()),
                    new TypeToken<List<ItemsModel>>() {
                    }.getType());
            items = gson.fromJson(builder.create().toJson(syncDataModel.getItems()),
                    new TypeToken<List<ItemsModel>>() {
                    }.getType());

            Log.e("prics-servics-typs-itms", "" + prices.size() + "-" + services.size() + "-" +
                    servicestypes.size() + "-" + items.size());


            for (int i = 0; i < prices.size(); i++) {
                ItemsModel item, service, serviceType;
                String id="";
                String ArabicName="";
                String EnglishName="";
                String ServiceEnglishName="";
                String ServiceArabicName="";
                String ServiceTypeArabicName="";
                String ServiceTypeEnglishName="";
                String unitePrice="";

                Log.e("Service Name", prices.get(i).getServiceTypeId() + " " + servicestypes.size() + " " + servicestypes.get(0).getId());
                item = SearchItems(prices.get(i).getItem(), items);
                service = SearchItems(prices.get(i).getServiceId(), services);
                serviceType = SearchItems(prices.get(i).getServiceTypeId(), servicestypes);

                id = "" + i;
                if (item != null) {
                    ArabicName = item.getArabicName();
                    EnglishName = item.getEnglishName();
                }
                if (service != null) {
                    ServiceEnglishName = service.getEnglishName();
                    ServiceArabicName = service.getArabicName();
                }
                if (serviceType != null) {

                    ServiceTypeArabicName = serviceType.getArabicName();
                    ServiceTypeEnglishName = serviceType.getEnglishName();
                }

                unitePrice = prices.get(i).getPrice();

                priceitems.add(new ItemsModel(id, ArabicName, EnglishName, ServiceEnglishName, ServiceArabicName,
                        ServiceTypeArabicName, ServiceTypeEnglishName, unitePrice));
            }
            return priceitems;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public ItemsModel SearchItems(String id, List<ItemsModel> items) {
        ItemsModel itemsModel = null;


        for (int i = 0; i < items.size(); i++) {
            Log.v("Id-ItemId", id + " - " + items.get(i).getId());
            id = id.replace(".0", "");
            String itemId = items.get(i).getId().replace(".0", "");
            if (itemId != null && itemId.equals(id)) {
//                itemsModel = items.get(i);
                return items.get(i);
            }
        }
        return null;
    }

    @Override
    public void onResponse(Object response) {
        Log.e("response", response.toString());
        try {
//            if (tag.equals(GetCatTAG)) {
            if (CheckResponse.getInstance().checkResponse(getActivity(), response.toString(), true)) {
                Log.e("response", "true");
                List<ItemsModel> priceOrders = decodeJson(response.toString());
                Log.e("priceOrders List size", "" + priceOrders.size());


                adapter = new PricesAdapter(con, priceOrders);
                LinearLayoutManager timesLayoutManager = new LinearLayoutManager(con, LinearLayoutManager.VERTICAL, false);
                prices.setLayoutManager(timesLayoutManager);
//                RecyclerView.LayoutManager mLayoutManager = new  GridLayoutManager(MainActivity.this, 2);
//                recyclerView.setLayoutManager(mLayoutManager);
//                recyclerView.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(10), true));
                prices.setItemAnimator(new DefaultItemAnimator());
                prices.setAdapter(adapter);
            }
        } catch (Exception e) {
            if (ConnectionVolley.dialog.isShowing()) {
                ConnectionVolley.dialog.dismiss();

            }
//            Toast.makeText(con, con.getResources().getString(R.string.connectionError), Toast.LENGTH_LONG).show();
            Log.e("e.printStackTrace()", "" + e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        VolleyLog.d("responseError", "Error: " + error.getMessage());
        try {
            ConnectionVolley.dialog.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}





