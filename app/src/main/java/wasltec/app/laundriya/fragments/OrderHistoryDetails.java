package wasltec.app.laundriya.fragments;


import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import wasltec.app.laundriya.R;
import wasltec.app.laundriya.activities.HomeActivity;
import wasltec.app.laundriya.adapters.InvoiceAdapter;
import wasltec.app.laundriya.models.OrderModel;
import wasltec.app.laundriya.utils.GeneralClass;

/**
 * Created by raed on 24/04/2017.
 */

public class OrderHistoryDetails extends android.app.Fragment {
    Context con;
    Activity act;
    RecyclerView invoiceRecycler;
    InvoiceAdapter adapter;
    TextView order, orderNum, orderState, orderDeleviryDate, orderDate, total;
    wasltec.app.laundriya.models.OrderModel userOrder;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_order_history_details, container, false);

        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onActivityCreated(savedInstanceState);
        act = getActivity();
        con = getActivity();
        HomeActivity.fromOrderHistory=true;
        Bundle args = OrderHistoryDetails.this.getArguments();
        userOrder = (OrderModel) args.getSerializable("order");
        invoiceRecycler = (RecyclerView) getView().findViewById(R.id.recycler_invoice);
        order = (TextView) getView().findViewById(R.id.txt_order);
        orderDate = (TextView) getView().findViewById(R.id.txt_date);
        orderNum = (TextView) getView().findViewById(R.id.order_number);
        orderState = (TextView) getView().findViewById(R.id.order_state);
        orderDeleviryDate = (TextView) getView().findViewById(R.id.order_delivery_date);
        total = (TextView) getView().findViewById(R.id.total);
//        userOrder = OrderHistory.selectedItem;
        if (GeneralClass.language.equals("en")) {
            order.setText(userOrder.getCategories().toString().replace("[", "").replace("]", ""));
        } else if (GeneralClass.language.equals("ar")) {
            order.setText(userOrder.getCategories().toString().replace("[", "").replace("]", ""));
        }


        orderDate.setText("" + userOrder.getDate().replace("T", " "));
        orderNum.setText("" + userOrder.getId());
        orderState.setText("" + userOrder.getState());
        orderDeleviryDate.setText("" + userOrder.getDate().replace("T", " "));
        total.setText("" + userOrder.getCost() + " SAR");


        if (userOrder != null && userOrder.getItems().size() > 0) {

            adapter = new InvoiceAdapter(con, userOrder.getItems());
            LinearLayoutManager timesLayoutManager = new LinearLayoutManager(con, LinearLayoutManager.VERTICAL, false);
            invoiceRecycler.setLayoutManager(timesLayoutManager);
//                RecyclerView.LayoutManager mLayoutManager = new  GridLayoutManager(MainActivity.this, 2);
//                recyclerView.setLayoutManager(mLayoutManager);
//                recyclerView.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(10), true));
            invoiceRecycler.setItemAnimator(new DefaultItemAnimator());
            invoiceRecycler.setAdapter(adapter);
        }

    }

}
