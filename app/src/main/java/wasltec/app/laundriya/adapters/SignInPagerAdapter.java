package wasltec.app.laundriya.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import wasltec.app.laundriya.fragments.SignInFragment1;
import wasltec.app.laundriya.fragments.SignInFragment2;

/**
 * Created by raed on 24/04/2017.
 */

public class SignInPagerAdapter extends FragmentPagerAdapter {

    public SignInPagerAdapter(FragmentManager supportFragmentManager) {
        super(supportFragmentManager);
    }

    // Returns the fragment to display for that page
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new SignInFragment1();

            case 1:
                return new SignInFragment2();


            default:
                return null;
        }
    }

    // Returns total number of pages
    @Override
    public int getCount() {
        return 2;
    }

}