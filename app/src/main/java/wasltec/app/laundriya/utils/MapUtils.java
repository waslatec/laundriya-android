package wasltec.app.laundriya.utils;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;
import java.util.Locale;

import wasltec.app.laundriya.R;
import wasltec.app.laundriya.models.AdressForLocatin;
import wasltec.app.laundriya.fragments.HomeFragment;

/**
 * Created by raed on 04/05/2017.
 */

public class MapUtils {

    //    Double myLat, myLng;
    AdressForLocatin myaddress = new AdressForLocatin("", "");
    AdressForLocatin scndAddress = new AdressForLocatin("", "");
    boolean scndLocation;
    Permissions permissions;
    GPSTracker gpsTracker;

    OnMapReadyCallback callback;
    GoogleMap.OnMarkerClickListener markerClicked;
    GoogleMap.OnMarkerDragListener onMarkerDragListener;
    MarkerOptions markerOption = null;
    Marker myMarker = null, scndMarker = null;
    MarkerOptions scnMarkerOptions = null;
    MapFragment mapFragment;
    final static public int LOCATION_PERMISSION = 180;


    public void initMapUtils(final Context con, final MapFragment mMapFragment,
                             final OnAddressRecievedListner listner) {
        gpsTracker = new GPSTracker(con);
        permissions = new Permissions();
        mapFragment = mMapFragment;
        onMarkerDragListener = new GoogleMap.OnMarkerDragListener() {
            @Override
            public void onMarkerDragStart(Marker marker) {

            }

            @Override
            public void onMarkerDrag(Marker marker) {


            }

            @Override
            public void onMarkerDragEnd(Marker marker) {
                myaddress.customLocation = true;
                myaddress.lat = marker.getPosition().latitude;
                myaddress.lng = marker.getPosition().longitude;
                mMapFragment.getMapAsync(MapUtils.this.callback);
            }
        };

        markerClicked = new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {

                return false;
            }
        };

        this.callback = new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                try {
                    if (scndLocation) {
                        LatLng latLng = new LatLng(scndAddress.lat, scndAddress.lng);
                        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15.0f));
                        googleMap.setOnMarkerClickListener(markerClicked);
                        googleMap.setOnMarkerDragListener(onMarkerDragListener);
                        googleMap.getCameraPosition();
                    }
                    AdressForLocatin addressForHome = GetAddress(con, myaddress.lat, myaddress.lng);
                    if (addressForHome != null) {
                        addressForHome.lat = myaddress.lat;
                        addressForHome.lng = myaddress.lng;
                        listner.onObjectReady(addressForHome);

                    }
                    LatLng latLngSrc = new LatLng(myaddress.lat, myaddress.lng);

                    if (markerOption == null) {
                        markerOption = new MarkerOptions()
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.my_location_marker))
                                .position(latLngSrc)
                                .draggable(true);
                        myMarker = googleMap.addMarker(markerOption);

                    } else {
                        if (myMarker != null) {
                            myMarker.setPosition(latLngSrc);
                        }
                    }

                    if (scndLocation) {
                        LatLng latLngDes = new LatLng(scndAddress.lat, scndAddress.lng);
                        if (scnMarkerOptions == null) {
                            scnMarkerOptions = new MarkerOptions()
                                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.driver_marker))
                                    .position(latLngDes);
                            googleMap.addMarker(scnMarkerOptions);
                        } else {
                            if (scndMarker != null) {
                                scndMarker.setPosition(latLngDes);
                            }
                        }
                    }
                    googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLngSrc, 15.0f));
                    googleMap.setOnMarkerClickListener(markerClicked);
                    googleMap.setOnMarkerDragListener(onMarkerDragListener);
                    googleMap.getCameraPosition();

                    googleMap.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {
                        @Override
                        public void onMyLocationChange(Location location) {
                            if (!myaddress.customLocation) {
                                myaddress.lat = location.getLatitude();
                                myaddress.lng = location.getLongitude();
                                mMapFragment.getMapAsync(MapUtils.this.callback);
                            }
                        }
                    });


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
    }

    public void setMyLocation(double lat, double lng) {
        scndLocation=false;
        myaddress.customLocation = true;
        myaddress.lat = lat;
        myaddress.lng = lng;
        mapFragment.getMapAsync(this.callback);

    }

    public void set2ndLocation(double lat, double lng) {
        scndLocation=true;
        scndAddress.customLocation = true;
        scndAddress.lat = lat;
        scndAddress.lng = lng;
        mapFragment.getMapAsync(this.callback);

    }


    public void getCurrentLocation(Activity act, final Context con, final MapFragment mMapFragment,
                                   final OnAddressRecievedListner listner) {


        initMapUtils(con, mMapFragment, listner);
        if (permissions.hasPermission(act, Manifest.permission.ACCESS_FINE_LOCATION)) {
            if (gpsTracker.canGetLocation()) {

                if (!myaddress.customLocation) {
                    myaddress.lat = gpsTracker.getLatitude();
                    myaddress.lng = gpsTracker.getLongitude();
                    Log.e("getCurrentLocation", "lat: " + myaddress.lat + " lng: " + myaddress.lng);
                    mapFragment.getMapAsync(this.callback);
                }

            } else {
                gpsTracker.showSettingsAlert();
            }
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                act.requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_PERMISSION);
            }
        }


    }


    public AdressForLocatin GetAddress(Context con, double lat, double lon) {
        try {
            Geocoder geo = new Geocoder(con, Locale.getDefault());
            List<Address> addresses = geo.getFromLocation(lat, lon, 1);
            if (addresses.isEmpty()) {
//                Toast.makeText(con, "Waiting for Location", Toast.LENGTH_LONG).show();
                return null;
            } else {
                if (addresses.size() > 0) {
                    String locality=addresses.get(0).getLocality();
                    String adminArea=addresses.get(0).getAdminArea();
                    String countryName=addresses.get(0).getCountryName();
                    String subAddress="";
                    if(locality!=null){
                        subAddress=subAddress+locality+",";
                    }
                    if(adminArea!=null){
                        subAddress=subAddress+adminArea+",";
                    }
                    if(countryName!=null){
                        subAddress=subAddress+countryName;
                    }

                    return new AdressForLocatin("" + addresses.get(0).getFeatureName() + ", " + addresses.get(0).getAddressLine(0),
                            subAddress);
                    //Toast.makeText(getApplicationContext(), "Address:- " + addresses.get(0).getFeatureName() + addresses.get(0).getAdminArea() + addresses.get(0).getLocality(), Toast.LENGTH_LONG).show();
                } else {
                    return null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace(); // getFromLocation() may sometimes fail
            return null;
        }

    }

    public interface OnAddressRecievedListner {
        // These methods are the different events and
        // need to pass relevant arguments related to the event triggered
        public void onObjectReady(AdressForLocatin address);

    }

}
