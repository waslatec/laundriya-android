package wasltec.app.laundriya.adapters;

import android.content.Context;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import wasltec.app.laundriya.R;
import wasltec.app.laundriya.fragments.NavigationDrawerFragment;

public class DaysAdapter
        extends RecyclerView.Adapter<DaysAdapter.SimpleViewHolder> {

    private final Context mContext;
    private List<Date> mData;
    OnItemClickedListener listner;
    public int isSelected = 0;

    public DaysAdapter(Context context, List<Date> data, OnItemClickedListener listener) {
        mContext = context;
        if (data != null)
            mData = data;
        else mData = new ArrayList<>();
        this.listner = listener;
    }

    @Override
    public int getItemViewType(int position) {
//        return mData.get(position).isHeader ? 1 : 2;
        return 2;
    }

    public void add(Date s, int position) {
        position = position == -1 ? getItemCount() : position;
        mData.add(position, s);
        notifyItemInserted(position);
    }


    public void remove(int position) {
        if (position < getItemCount()) {
            mData.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void refresh() {
        notifyDataSetChanged();
    }

    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view;
//        if (viewType == 1)
//            view = LayoutInflater.from(mContext).inflate(R.layout.menu_section, parent, false);
//        else
        view = LayoutInflater.from(mContext).inflate(R.layout.day_item, parent, false);

        return new SimpleViewHolder(view, viewType);
    }

    @Override
    public void onBindViewHolder(final SimpleViewHolder holder, final int position) {
        final Date item = mData.get(position);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(item);
        String dateName = calendar.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.SHORT, Locale.US);

        holder.date.setText("" + item.getDay());
        holder.day.setText("" + dateName);
        int width=(int) mContext.getResources().getDimension(R.dimen._34sdp);
        int hight=(int) mContext.getResources().getDimension(R.dimen._30sdp);

        holder.container.setLayoutParams(new LinearLayout.LayoutParams(width,hight));
        if (isSelected == position) {

            holder.container.setBackgroundResource(R.drawable.item_bg_activated);
            holder.day.setTextColor(mContext.getResources().getColor(R.color.lightBlue));
            holder.date.setTextColor(mContext.getResources().getColor(R.color.lightBlue));

        } else {
            holder.container.setBackgroundResource(R.drawable.item_bg);
            holder.day.setTextColor(mContext.getResources().getColor(R.color.grey));
            holder.date.setTextColor(mContext.getResources().getColor(R.color.grey));
        }

        holder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                isSelected = position;
//                        setSelected(pos);
                holder.container.setBackgroundResource(R.drawable.item_bg);
                holder.day.setTextColor(mContext.getResources().getColor(R.color.lightBlue));
                holder.date.setTextColor(mContext.getResources().getColor(R.color.lightBlue));
                listner.setOnItemClicked(item,position);
                notifyDataSetChanged();
            }

        });


    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public class SimpleViewHolder extends RecyclerView.ViewHolder {
        public final TextView date, day;
        public final LinearLayout container;

        public SimpleViewHolder(View view, int viewType) {
            super(view);
            day = (TextView) view.findViewById(R.id.txt_day);
            date = (TextView) view.findViewById(R.id.txt_date);
            container = (LinearLayout) view.findViewById(R.id.container);
        }
    }

//    public void setSelected(int position) {
//
//        for (int i = 0; i < mData.size(); i++) {
//            mData.get(i).isSelected = position == i;
//            notifyDataSetChanged();
//        }
//    }

    public interface OnItemClickedListener {
        // TODO: Update argument type and name

        void setOnItemClicked(Date item,int pos);
    }
}
