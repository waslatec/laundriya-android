package wasltec.app.laundriya.models;

import java.util.ArrayList;

/**
 * Created by Amr Heidar on 3/27/2017.
 */

public class times_model {

    ArrayList<String> Times;
    String Day;
    public boolean isSelected;

    public ArrayList<String> getTimes() {
        return Times;
    }

    public void setTimes(ArrayList<String> times) {
        Times = times;
    }

    public String getDay() {
        return Integer.toString(Double.valueOf(Day).intValue());
    }

    public void setDay(String day) {
        Day = day;
    }
}
