package wasltec.app.laundriya.models;

/**
 * Created by raed on 09/05/2017.
 */

public class PriceModel {

    private String Price;
    private String Item;
    private String serviceId;
    private String serviceTypeId;

    public PriceModel() {
    }

    public PriceModel(String price, String item, String serviceId, String serviceTypeId) {
        Price = price;
        Item = item;
        this.serviceId = serviceId;
        this.serviceTypeId = serviceTypeId;
    }

    public String getPrice() {
        return Price;
    }

    public void setPrice(String price) {
        Price = price;
    }

    public String getItem() {
        return Item;
    }

    public void setItem(String item) {
        Item = item;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getServiceTypeId() {
        return serviceTypeId;
    }

    public void setServiceTypeId(String serviceTypeId) {
        this.serviceTypeId = serviceTypeId;
    }
}
