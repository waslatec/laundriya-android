package wasltec.app.laundriya.Dialoges;

import android.app.Activity;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

import wasltec.app.laundriya.DataAccess.ServerCalls;
import wasltec.app.laundriya.R;
import wasltec.app.laundriya.activities.HomeActivity;
import wasltec.app.laundriya.adapters.InvoiceAdapter;
import wasltec.app.laundriya.fragments.HomeFragment;
import wasltec.app.laundriya.fragments.OrderHistoryDetails;
import wasltec.app.laundriya.hub.HubCalls;
import wasltec.app.laundriya.models.CategoryModel;
import wasltec.app.laundriya.models.OrderModel;
import wasltec.app.laundriya.models.StandardWebServiceResponse;
import wasltec.app.laundriya.serverconnection.volley.ConnectionVolley;
import wasltec.app.laundriya.utils.GeneralClass;

/**
 * Created by raed on 02/05/2017.
 */

public class OrderDetails extends Dialog implements Response.Listener, Response.ErrorListener {

    public Context con;
    public Activity act;
    public Dialog d;
    public LinearLayout orderdetails;
    ImageView cancel, confirm;
    String id;
    String tag;
    ArrayList<OrderModel> OrderModelArrayList;
    final String GetOrders = "GetOrders";

    public OrderDetails(Activity a, String id) {
        super(a);
        // TODO Auto-generated constructor stub
        this.con = a;
        this.act = a;
        this.id = id;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().setBackgroundDrawable(new ColorDrawable(act.getResources().getColor(R.color.tran_blue)));
//        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_orderdetails);

        WindowManager wm = (WindowManager) act.getApplicationContext().getSystemService(act.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        int width = display.getWidth(); // deprecated
        int height = display.getHeight(); // deprecated
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(act.getWindow().getAttributes());
        lp.width = width;
        lp.height = height;
        getWindow().setAttributes((new WindowManager.LayoutParams(lp.width, lp.height)));

        orderdetails = (LinearLayout) findViewById(R.id.orderdetails);
        cancel = (ImageView) findViewById(R.id.cancel);
        confirm = (ImageView) findViewById(R.id.confirm);

        GetOrders();

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OrderDetails.this.dismiss();
            }
        });

        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OrderDetails.this.dismiss();
                HubCalls.ConfirmDriverTakeItems(id);
            }
        });


    }


    public interface OnOrderAcceptClickListener {
        void setOnAcceptClick();
    }


    void GetOrders() {
        OrderModelArrayList = new ArrayList<>();
        ServerCalls calls = new ServerCalls();
        tag = GetOrders;
        ConnectionVolley connectionVolley = null;
        calls.callServerToGetItems(con, this, this, connectionVolley);


    }

    private void decodeJson(String jsonString) {
        try {
            Log.d("JSONResponse2", jsonString);

            Gson gson = new Gson();
            StandardWebServiceResponse standardWebServiceResponse = new StandardWebServiceResponse();
            gson = new Gson();
            standardWebServiceResponse = gson.fromJson(jsonString, StandardWebServiceResponse.class);
            OrderModelArrayList = new ArrayList<>();
            gson = new Gson();
            GsonBuilder builder = new GsonBuilder().excludeFieldsWithoutExposeAnnotation();

            OrderModelArrayList = gson.fromJson(builder.create().toJson(standardWebServiceResponse.getOrdersList()), new TypeToken<List<OrderModel>>() {
            }.getType());
            OrderModel order = null;
            for (int i = 0; i < OrderModelArrayList.size(); i++) {
                if (OrderModelArrayList.get(i).getId().equals(id)) {
                    order = OrderModelArrayList.get(i);
                }
            }
//            OrderHistoryDetails details = new OrderHistoryDetails();
//            Bundle args = new Bundle();
//            args.putSerializable("order", order);
//            details.setArguments(args);
//            FragmentTransaction ft = act.getFragmentManager()
//                    .beginTransaction();
//            ft.replace(R.id.orderdetails, details).commit();

            tempOrderDetails(order);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private void tempOrderDetails(OrderModel userOrder) {

        RecyclerView invoiceRecycler;
        InvoiceAdapter adapter;
        TextView order, orderNum, orderState, orderDeleviryDate, orderDate, total;
        invoiceRecycler = (RecyclerView) findViewById(R.id.recycler_invoice);
        order = (TextView) findViewById(R.id.txt_order);
        orderDate = (TextView) findViewById(R.id.txt_date);
        orderNum = (TextView) findViewById(R.id.order_number);
        orderState = (TextView) findViewById(R.id.order_state);
        orderDeleviryDate = (TextView) findViewById(R.id.order_delivery_date);
        total = (TextView) findViewById(R.id.total);
//        userOrder = OrderHistory.selectedItem;
        if (GeneralClass.language.equals("en")) {
            order.setText(userOrder.getCategories().toString().replace("[", "").replace("]", ""));
        } else if (GeneralClass.language.equals("ar")) {
            order.setText(userOrder.getCategories().toString().replace("[", "").replace("]", ""));
        }


        orderDate.setText("" + userOrder.getDate().replace("T", " "));
        orderNum.setText("" + userOrder.getId());
        orderState.setText("" + userOrder.getState());
        orderDeleviryDate.setText("" + userOrder.getDate().replace("T", " "));
        total.setText("" + userOrder.getCost() + " SAR");


        if (userOrder != null && userOrder.getItems().size() > 0) {

            adapter = new InvoiceAdapter(con, userOrder.getItems());
            LinearLayoutManager timesLayoutManager = new LinearLayoutManager(con, LinearLayoutManager.VERTICAL, false);
            invoiceRecycler.setLayoutManager(timesLayoutManager);
//                RecyclerView.LayoutManager mLayoutManager = new  GridLayoutManager(MainActivity.this, 2);
//                recyclerView.setLayoutManager(mLayoutManager);
//                recyclerView.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(10), true));
            invoiceRecycler.setItemAnimator(new DefaultItemAnimator());
            invoiceRecycler.setAdapter(adapter);
        }

    }


    @Override
    public void onResponse(Object response) {
        Log.e("response", response.toString());
        try {
            decodeJson(response.toString());
//            decodeJson2(response.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        VolleyLog.d("responseError", "Error: " + error.getMessage());
        try {
            ConnectionVolley.dialog.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
