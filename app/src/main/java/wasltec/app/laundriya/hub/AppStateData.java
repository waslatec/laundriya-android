package wasltec.app.laundriya.hub;

import android.content.Context;
import android.util.Log;

/**
 * Created by Wasltec on 03/03/2016.
 */
public class AppStateData
{
    public static Context mContext;
    public  static AppStateData mAppStateContext=null;
    private final  static String HUB_TAG="hubReceiver";

    public AppStateData(Context mContext)
    {
        AppStateData.mContext =mContext;
    }

    public static AppStateData getInstance( Context mcontext)
    {
        mContext=mcontext;
        if (mAppStateContext == null)
        {
            mAppStateContext = new AppStateData(mcontext);
        }
        return mAppStateContext;
    }   // getInstance()

    public void RejectedOrderWithRef(int refId,String reason)
    {
        Log.v(HUB_TAG," RejectedOrderWithRef "+refId +" "+reason) ;
        BroadcastHelper.sendInform(BroadcastHelper.ORDER_REJECTED_BROADCAST_METHOD, mContext);
    }

    public void PendingOrder(int refId,int OrderId)
    {
        Log.v(HUB_TAG," PendingOrder "+refId +" "+OrderId) ;
        AppConfigHelper.signalR_order_id = OrderId;
        BroadcastHelper.sendInform(BroadcastHelper.PENDING_ORDER_BROADCAST_METHOD, mContext);
    }

    public void orderConfirmed(int OrderId)
    {
        Log.v(HUB_TAG," orderConfirmed  "+OrderId) ;
        AppConfigHelper.signalR_order_id = OrderId;
        BroadcastHelper.sendInform(BroadcastHelper.ORDER_CONFIRMED_BROADCAST_METHOD, mContext);
    }

    public void OrderCooked(int OrderId)
    {
        Log.v(HUB_TAG, " OrderCooked " + OrderId) ;
        AppConfigHelper.signalR_order_id = OrderId;
        BroadcastHelper.sendInform(BroadcastHelper.ORDER_COOKED_BROADCAST_METHOD, mContext);
    }

    public void OrderStartDelivering(int OrderId,int driverId)
    {
        Log.v(HUB_TAG, " OrderStartDelivering " + OrderId + " " + driverId) ;
        AppConfigHelper.signalR_order_id = OrderId;
        BroadcastHelper.sendInform(BroadcastHelper.ORDER_START_DEVLEVIRING_BROADCAST_METHOD, mContext);
    }

    public void driverUpdate(int driverId, Double lat, Double lng)
    //public void driverupdate(String englishName , String arabicName , int driverId, double lat, double lng)
    {
        Log.v(HUB_TAG, " driverUpdate " + driverId +" "+lat+" "+lng) ;
        AppConfigHelper.driver_lat = lat;
        AppConfigHelper.driver_lng = lng;
        BroadcastHelper.sendInform(BroadcastHelper.DRIVER_UPDATE_BROADCAST_METHOD, mContext);
    }

    public void OrderDeliveried(int OrderId)
    {
        Log.v(HUB_TAG, " OrderDeliveried " + OrderId) ;
        AppConfigHelper.signalR_order_id = OrderId;
        BroadcastHelper.sendInform(BroadcastHelper.ORDER_DELIVERED_BROADCAST_METHOD, mContext);
    }
    /*****
     * From Driver application will send you

     5)OrderStartDelivering(OrderId, order.DriverId)

     6)driverupdate(connectedDriver.EnglishName , connectedDriver.ArabicName , connectedDriver.DriverId, lat, lng);>>display on map

     7)OrderDeliveried(order.Id)
     */
}       // AppStateData CLASS