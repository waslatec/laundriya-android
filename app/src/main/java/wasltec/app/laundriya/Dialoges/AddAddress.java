package wasltec.app.laundriya.Dialoges;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;
import java.util.Locale;

import wasltec.app.laundriya.R;
import wasltec.app.laundriya.activities.HomeActivity;

/**
 * Created by raed on 02/05/2017.
 */

public class AddAddress extends Dialog {

    public Activity c;
    public Dialog d;
    public TextView address;
    EditText address_details;
    Button confirm;
    String addressDetails, addressStr = "0";
    double lat, lng;
    private final OnConfirmClickListener listener;

    public AddAddress(Activity a, double lat, double lng, OnConfirmClickListener listener) {
        super(a);
        // TODO Auto-generated constructor stub
        this.c = a;
        this.listener = listener;
        this.lat = lat;
        this.lng = lng;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().setBackgroundDrawable(new ColorDrawable(c.getResources().getColor(R.color.trans)));
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_add_address);
//        WindowManager wm = (WindowManager) c.getApplicationContext().getSystemService(c.WINDOW_SERVICE);
//        Display display = wm.getDefaultDisplay();
//        int width = display.getWidth(); // deprecated
//        int height = display.getHeight(); // deprecated
//        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
//        lp.copyFrom(c.getWindow().getAttributes());
//        lp.width = width;
//        lp.height = height;
//        getWindow().setAttributes((new WindowManager.LayoutParams(lp.width, lp.height)));
        confirm = (Button) findViewById(R.id.confirm);
        address_details = (EditText) findViewById(R.id.address_details);
        address_details.setFocusableInTouchMode(true);
        address = (TextView) findViewById(R.id.address);
        addressStr = getAddress(c, lat, lng);
        address.setText("" + addressStr);

        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Processing processing = new Processing(c);
                AddAddress.this.dismiss();
//                HomeActivity.SwitchToHome();
                addressDetails = "" + address_details.getText().toString();
                listener.setOnConfirmClick(lat, lng, addressStr, addressDetails);
            }
        });


    }

    public String getAddress(Activity act, double latitude, double longitude) {

        String strAdd = "";
        Geocoder geocoder = new Geocoder(act, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("");

                for (int i = 0; i < returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                strAdd = strReturnedAddress.toString();
                //Log.w("My Current loction address", "" + strReturnedAddress.toString());
            } else {
                //Log.w("My Current loction address", "No Address returned!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            // Log.w("My Current loction address", "Canont get Address!");
        }
        return strAdd;
    }

    public interface OnConfirmClickListener {
        void setOnConfirmClick(double lat, double lng, String addressStr, String addressDetails);
    }


    @Override
    public void onBackPressed() {

        AddAddress.this.dismiss();
    }
}
