package wasltec.app.laundriya.models;

import java.io.Serializable;

import wasltec.app.laundriya.utils.GeneralClass;

/**
 * Created by Amr Heidar on 2/26/2017.
 */
public class AddressModel implements Serializable {

    private String Id;
    private String AddressHint;
    private String ClientId;
    private String DeliveryAddress;
    private String FlatNumber;
    private String FloorNumber;
    private String Latitude;
    private String Logitude;

    public String getId() {
        return Integer.toString(Double.valueOf(Id).intValue());
    }

    public void setId(String id) {
        Id = id;
    }

    public String getAddressHint() {
        return AddressHint;
    }

    public void setAddressHint(String addressHint) {
        AddressHint = addressHint;
    }

    public String getClientId() {
        return ClientId;
    }

    public void setClientId(String clientId) {
        ClientId = clientId;
    }

    public String getDeliveryAddress() {
        return DeliveryAddress;
    }

    public void setDeliveryAddress(String deliveryAddress) {
        DeliveryAddress = deliveryAddress;
    }

    public String getFlatNumber() {
        return FlatNumber;
    }

    public void setFlatNumber(String flatNumber) {
        FlatNumber = flatNumber;
    }

    public String getFloorNumber() {
        return FloorNumber;
    }

    public void setFloorNumber(String floorNumber) {
        FloorNumber = floorNumber;
    }

    public String getLatitude() {
        return Latitude;
    }

    public void setLatitude(String latitude) {
        Latitude = latitude;
    }

    public String getLogitude() {
        return Logitude;
    }

    public void setLogitude(String logitude) {
        Logitude = logitude;
    }

    public String toString(){
        if(GeneralClass.language.equals("en"))
            return getDeliveryAddress();
        else
            return getDeliveryAddress();
    }
}
