package wasltec.app.laundriya.adapters;

//import wasltec.app.laundriya.models.DeliveryDate;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import wasltec.app.laundriya.R;
import wasltec.app.laundriya.fragments.NavigationDrawerFragment;
import wasltec.app.laundriya.models.CategoryModel;
import wasltec.app.laundriya.serverconnection.Url;
import wasltec.app.laundriya.utils.GeneralClass;

public class CategoriesAdapter extends RecyclerView.Adapter<CategoriesAdapter.SimpleViewHolder> {

    private final Context mContext;
    private List<CategoryModel> mData;
    private final OnItemClickListener listener;


    public CategoriesAdapter(Context context, List<CategoryModel> data, OnItemClickListener listener) {
        this.mContext = context;
        this.listener = listener;
        if (data != null)
            this.mData = data;
        else this.mData = new ArrayList<>();
    }

    @Override
    public int getItemViewType(int position) {
//        return mData.get(position).isHeader ? 1 : 2;
        return 2;
    }

    public void add(CategoryModel s, int position) {
        position = position == -1 ? getItemCount() : position;
        mData.add(position, s);
        notifyItemInserted(position);
    }


    public void remove(int position) {
        if (position < getItemCount()) {
            mData.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void refresh() {
        notifyDataSetChanged();
    }

    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view;
//        if (viewType == 1)
//            view = LayoutInflater.from(mContext).inflate(R.layout.menu_section, parent, false);
//        else
        view = LayoutInflater.from(mContext).inflate(R.layout.category_item, parent, false);

        return new SimpleViewHolder(view, viewType);
    }

    @Override
    public void onBindViewHolder(SimpleViewHolder holder, final int position) {

        final CategoryModel item = mData.get(position);

        Picasso.with(mContext)
                .load(Url.getInstance().categoriesImageURL + item.getId() + ".png")
                .placeholder(R.mipmap.ic_launcher)
                .into(holder.imgCategory);


        if (GeneralClass.language.equals("en")) {
            holder.txtCategory.setText(item.getEnglishName());
        } else if (GeneralClass.language.equals("ar")) {
            holder.txtCategory.setText(item.getArabicName());
        }
        if (item.isSelected) {

            holder.container.setBackgroundResource(R.drawable.item_bg_activated);
            holder.txtCategory.setTextColor(mContext.getResources().getColor(R.color.lightBlue));


        } else {
            holder.container.setBackgroundResource(R.drawable.item_bg);
            holder.txtCategory.setTextColor(mContext.getResources().getColor(R.color.grey));

        }

        holder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(item,position);

            }
        });

    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public class SimpleViewHolder extends RecyclerView.ViewHolder {
        public final TextView txtCategory;
        public final ImageView imgCategory;
        public final LinearLayout container;

        public SimpleViewHolder(View view, int viewType) {
            super(view);
            txtCategory = (TextView) view.findViewById(R.id.txt_category);
            imgCategory = (ImageView) view.findViewById(R.id.img_category);
            container = (LinearLayout) view.findViewById(R.id.container);
           int w= (int) mContext.getResources().getDimension(R.dimen._54sdp);
            int h=(int)mContext.getResources().getDimension(R.dimen._50sdp);
            container.setLayoutParams(new LinearLayout.LayoutParams(w,h));
//            if (viewType == 2)
//                view.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        int pos = getLayoutPosition();
//                        setSelected(pos);
//                        container.setBackgroundResource(R.drawable.item_bg);
//                        txtCategory.setTextColor(mContext.getResources().getColor(R.color.lightBlue));
////                        imgCategory.setImageResource(R.drawable.);
//
//                        notifyDataSetChanged();
//                    }
//
//                });
        }
    }

    public void setSelected(int position) {
        for (int i = 0; i < mData.size(); i++) {
            mData.get(i).isSelected = position == i;
            notifyDataSetChanged();
        }
    }

    public interface OnItemClickListener {
        void onItemClick(CategoryModel item,int pos);
    }
}
