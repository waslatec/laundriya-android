package wasltec.app.laundriya.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

//import wasltec.app.laundriya.models.DeliveryDate;
//import wasltec.app.laundriya.models.InvoiceItem;
import wasltec.app.laundriya.R;
import wasltec.app.laundriya.models.ItemsModel;
import wasltec.app.laundriya.utils.GeneralClass;

public class InvoiceAdapter extends RecyclerView.Adapter<InvoiceAdapter.SimpleViewHolder> {

    private final Context mContext;
    private List<ItemsModel> mData;

    public InvoiceAdapter(Context context, List<ItemsModel> data) {
        mContext = context;
        if (data != null)
            mData = data;
        else mData = new ArrayList<>();
    }

    @Override
    public int getItemViewType(int position) {
//        return mData.get(position).isHeader ? 1 : 2;
        return 2;
    }

    public void add(ItemsModel s, int position) {
        position = position == -1 ? getItemCount() : position;
        mData.add(position, s);
        notifyItemInserted(position);
    }


    public void remove(int position) {
        if (position < getItemCount()) {
            mData.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void refresh() {
        notifyDataSetChanged();
    }

    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view;
//        if (viewType == 1)
//            view = LayoutInflater.from(mContext).inflate(R.layout.menu_section, parent, false);
//        else
        view = LayoutInflater.from(mContext).inflate(R.layout.invoice_item, parent, false);

        return new SimpleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SimpleViewHolder holder, final int position) {
        ItemsModel item = mData.get(position);
        double totalPric = 0;
        try {

            double price = Double.parseDouble(item.getUnitePrice());
            double qtyInt = Double.parseDouble(item.getAmount());
            totalPric = price * qtyInt;
        } catch (Exception e) {
            e.printStackTrace();
            totalPric = 0;
        }
        if (GeneralClass.language.equals("en")) {
            holder.item.setText(item.getEnglishName());
            holder.service.setText(item.getServiceEnglishName());
            holder.type.setText(item.getServiceTypeEnglishName());
        } else if (GeneralClass.language.equals("ar")) {
            holder.item.setText(item.getArabicName());
            holder.service.setText(item.getServiceArabicName());
            holder.type.setText(item.getServiceTypeArabicName());
        }


        holder.qty.setText(item.getAmount());
        holder.uprice.setText(item.getUnitePrice());
        holder.tprice.setText("" + totalPric);

//        if (item.isSelected) {
//
//            holder.container.setBackgroundResource(R.drawable.item_bg);
//            holder.day.setTextColor(mContext.getResources().getColor(R.color.lightBlue));
//            holder.date.setTextColor(mContext.getResources().getColor(R.color.lightBlue));
//
//        } else {
//            holder.container.setBackgroundResource(R.drawable.item_bg_activated);
//            holder.day.setTextColor(mContext.getResources().getColor(R.color.grey));
//            holder.date.setTextColor(mContext.getResources().getColor(R.color.grey));
//        }

    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public class SimpleViewHolder extends RecyclerView.ViewHolder {
        public final TextView item, service, type, qty, uprice, tprice;
        public final LinearLayout container;

        public SimpleViewHolder(View view) {
            super(view);
            item = (TextView) view.findViewById(R.id.item);
            service = (TextView) view.findViewById(R.id.service);
            type = (TextView) view.findViewById(R.id.type);
            qty = (TextView) view.findViewById(R.id.qty);
            uprice = (TextView) view.findViewById(R.id.u_price);
            tprice = (TextView) view.findViewById(R.id.t_price);
            container = (LinearLayout) view.findViewById(R.id.container);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int pos = getLayoutPosition();
//                        setSelected(pos);
//                        container.setBackgroundResource(R.drawable.item_bg);
//                        item.setTextColor(mContext.getResources().getColor(R.color.lightBlue));
//

                    notifyDataSetChanged();
                }

            });
        }
    }

//    public void setSelected(int position) {
//        for (int i = 0; i < mData.size(); i++) {
//            mData.get(i).isSelected = position == i;
//            notifyDataSetChanged();
//        }
//    }


}
