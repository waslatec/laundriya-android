package wasltec.app.laundriya.models;

import java.io.Serializable;

/**
 * Created by ahmed on 2/23/17.
 */

public class UserModel implements Serializable {

    //    private String Mobilenumber = "";
    private String Code ;
    private String username;
    private String password;
    private String MobileNumber;
    private String Email ;
    private String City ;
    private String Latitude ;
    private String Logitude ;
    private String AddressHint;
    private String DeliveryAddress ;

    public String getMobilenumber() {
        return MobileNumber;
    }

    public void setMobilenumber(String mobilenumber) {
        MobileNumber = mobilenumber;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String code) {
        Code = code;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMobileNumber() {
        return MobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        MobileNumber = mobileNumber;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public String getLatitude() {
        return Latitude;
    }

    public void setLatitude(String latitude) {
        Latitude = latitude;
    }

    public String getLogitude() {
        return Logitude;
    }

    public void setLogitude(String logitude) {
        Logitude = logitude;
    }

    public String getAddressHint() {
        return AddressHint;
    }

    public void setAddressHint(String addressHint) {
        AddressHint = addressHint;
    }

    public String getDeliveryAddress() {
        return DeliveryAddress;
    }

    public void setDeliveryAddress(String deliveryAddress) {
        DeliveryAddress = deliveryAddress;
    }
}
