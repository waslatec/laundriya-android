package wasltec.app.laundriya.models;

import java.util.Date;

/**
 * Created by raed on 02/05/2017.
 */

public class DeliveryDate {

    String dayOfWeak, day, month, dateStr;
    DeliveryTime time;
    Date date;
    public boolean isSelected = false;

    public DeliveryDate(Date date) {
        this.date = date;
    }

    public DeliveryDate(String dayOfWeak, String day, String month) {
        this.dayOfWeak = dayOfWeak;
        this.day = day;
        this.month = month;
    }

    public DeliveryDate(String dayOfWeak, String day, String month, String dateStr, Date date) {
        this.dayOfWeak = dayOfWeak;
        this.day = day;
        this.month = month;
        this.dateStr = dateStr;
        this.date = date;
    }

    public DeliveryDate(String dayOfWeak, String day, String month, String dateStr, DeliveryTime time) {
        this.dayOfWeak = dayOfWeak;
        this.day = day;
        this.month = month;
        this.dateStr = dateStr;
        this.time = time;
    }

    public DeliveryTime getTime() {
        return time;
    }

    public void setTime(DeliveryTime time) {
        this.time = time;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setDayOfWeak(String dayOfWeak) {
        this.dayOfWeak = dayOfWeak;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public void setMonth(String month) {
        this.month = month;
    }


    public void setDateStr(String dateStr) {
        this.dateStr = dateStr;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getDayOfWeak() {
        return dayOfWeak;
    }

    public String getDay() {
        return day;
    }

    public String getMonth() {
        return month;
    }

    public String getDateStr() {
        return dateStr;
    }

    public Date getDate() {
        return date;
    }

    public DeliveryDate() {

    }
}
