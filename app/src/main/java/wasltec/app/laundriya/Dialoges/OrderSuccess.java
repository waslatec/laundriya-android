package wasltec.app.laundriya.Dialoges;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import wasltec.app.laundriya.R;
import wasltec.app.laundriya.activities.HomeActivity;

/**
 * Created by raed on 02/05/2017.
 */

public class OrderSuccess extends Dialog {

    public Activity c;
    public Dialog d;
    public ImageView img_track, img_home;
    public LinearLayout lin_track, lin_home;
    public TextView txt_track, txt_home;
    String orderId = "0";

    public OrderSuccess(Activity a, String id) {
        super(a);
        // TODO Auto-generated constructor stub
        this.c = a;
        this.orderId = id;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().setBackgroundDrawable(new ColorDrawable(c.getResources().getColor(R.color.tran_blue)));
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_order_success);
        WindowManager wm = (WindowManager) c.getApplicationContext().getSystemService(c.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        int width = display.getWidth(); // deprecated
        int height = display.getHeight(); // deprecated
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(c.getWindow().getAttributes());
        lp.width = width;
        lp.height = height;
        getWindow().setAttributes((new WindowManager.LayoutParams(lp.width, lp.height)));
        lin_track = (LinearLayout) findViewById(R.id.lin_track);
        lin_home = (LinearLayout) findViewById(R.id.lin_home);
        img_track = (ImageView) findViewById(R.id.img_track);
        img_home = (ImageView) findViewById(R.id.img_home);
        txt_track = (TextView) findViewById(R.id.txt_track);
        txt_home = (TextView) findViewById(R.id.txt_home);

        lin_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OrderSuccess.this.dismiss();
                HomeActivity.SwitchToHome();
            }
        });

        lin_track.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Processing processing = new Processing(c);
                OrderSuccess.this.dismiss();
                HomeActivity.SwitchFragmentTrackOrder(orderId);

            }
        });


    }


}
