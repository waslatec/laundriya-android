package wasltec.app.laundriya.fragments;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.InputFilter;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;

import wasltec.app.laundriya.DataAccess.ServerCalls;
import wasltec.app.laundriya.R;
import wasltec.app.laundriya.activities.HomeActivity;
import wasltec.app.laundriya.activities.Register;
import wasltec.app.laundriya.serverconnection.volley.ConnectionVolley;
import wasltec.app.laundriya.utils.CheckResponse;
import wasltec.app.laundriya.utils.Validator;

/**
 * Created by raed on 24/04/2017.
 */

public class RegisterFragment2 extends Fragment implements Response.Listener, Response.ErrorListener {

    Context con;
    Activity act;
    EditText phone;
    LinearLayout linNext, linPrev;
    Button btnNext, btnPrev;
    TextView txtNext, txtPrev;
    boolean verificationError = true;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_reg2, container, false);

        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onActivityCreated(savedInstanceState);
        act = getActivity();
        con = getActivity();
        final Validator valid=new Validator();
        phone = (EditText) getView().findViewById(R.id.edit_mobilr);

        linNext = (LinearLayout) getView().findViewById(R.id.lin_next);
        linPrev = (LinearLayout) getView().findViewById(R.id.lin_prev);

        btnNext = (Button) getView().findViewById(R.id.btn_next);
        btnPrev = (Button) getView().findViewById(R.id.btn_prev);

        txtNext = (TextView) getView().findViewById(R.id.txt_next);
        txtPrev = (TextView) getView().findViewById(R.id.txt_prev);


//        phone.setFilters(new InputFilter[]{valid.filter});

        linNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String phoneStr = phone.getText().toString();
                if (valid.validatePhone(phoneStr)) {
                    Register.user.setMobileNumber(phoneStr);
                    verificationError = false;

                } else {
                    verificationError = true;
                    Log.v("Validation Error", "Phone");
                }

                if (verificationError) {

                    Log.v("Validation Error", "Erroooooooooooooooor");

                } else {
                    ConnectionVolley ConnectionVolley=null;
                    ServerCalls calls = new ServerCalls();
                    calls.callServerToSignUp(Register.user.getUsername(), Register.user.getEmail(),
                            Register.user.getMobileNumber(), con, RegisterFragment2.this, RegisterFragment2.this,ConnectionVolley);

                }
            }
        });

        linPrev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Register.previouseTab();

            }
        });


    }




    //381415
    @Override
    public void onResponse(Object response) {
        Log.e("response tag:"+"Sign Up", response.toString());
        try {
            if (CheckResponse.getInstance().checkResponse(con, response.toString(), true)) {
                Register.NextTab();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        VolleyLog.d("responseError", "Error: " + error.getMessage());
        try {
            ConnectionVolley.dialog.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
