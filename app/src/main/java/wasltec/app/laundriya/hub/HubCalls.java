package wasltec.app.laundriya.hub;

import android.app.Dialog;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import microsoft.aspnet.signalr.client.hubs.SubscriptionHandler5;
import wasltec.app.laundriya.Dialoges.Order;
import wasltec.app.laundriya.Dialoges.OrderDetails;
import wasltec.app.laundriya.MyReceiver;
import wasltec.app.laundriya.MyReceiver2;
import wasltec.app.laundriya.R;
import wasltec.app.laundriya.activities.HomeActivity;
import wasltec.app.laundriya.fragments.HomeFragment;
import wasltec.app.laundriya.utils.GeneralClass;

import microsoft.aspnet.signalr.client.Action;
import microsoft.aspnet.signalr.client.ConnectionState;
import microsoft.aspnet.signalr.client.ErrorCallback;
import microsoft.aspnet.signalr.client.hubs.SubscriptionHandler1;
import microsoft.aspnet.signalr.client.hubs.SubscriptionHandler3;
import microsoft.aspnet.signalr.client.hubs.SubscriptionHandler4;

public class HubCalls {
    static Context mContext;
    private final static String HUB_TAG = "HubCalls";

    public HubCalls(Context mContext) {
        this.mContext = mContext;
    }

    public static void clientConnected(final Context context) {
        try {
            if (HubFactory.connection.getState() == ConnectionState.Connected && HubFactory.hub != null) {
                HubFactory.hub.invoke("clientConnected").done(new Action<Void>() {
                    @Override
                    public void run(Void aVoid) throws Exception {
                        Log.v(HUB_TAG, "ImConnected Ok 20");
                    }
                }).onError(new ErrorCallback() {
                    @Override
                    public void onError(Throwable throwable) {
                        Log.v(HUB_TAG, "ImConnected Error");
                        BroadcastHelper.sendInform(BroadcastHelper.HUB_CONNECTION_ERROR_BROADCAST_METHOD, context);
                    }
                });
            } else {
                BroadcastHelper.sendInform(BroadcastHelper.HUB_CONNECTION_ERROR_BROADCAST_METHOD, context);
            }


        } catch (Exception e) {
            e.printStackTrace();
            BroadcastHelper.sendInform(BroadcastHelper.HUB_CONNECTION_ERROR_BROADCAST_METHOD, context);
        }
    }   // clientConnected()


    public static void postOrder(final Context context, String OrderedLat, String OrderedLng,
                                 String AddressId, String Cost, String PaymentType, boolean IsSchedule, String ScheduleDate, boolean Now, String MobileNumber) {
        mContext = context;
        Log.v("rrrrrrrrrrrrrr", "afdafafdnuj");
        try {
            if (HubFactory.connection.getState() == ConnectionState.Connected && HubFactory.hub != null) {
                Log.v("djadbjfkaabfbdbfuufb", "Amr Heidar");
                // invoke(String.class, "iAmAvailable", "username", "password", "TransMedic")
                HubFactory.hub.invoke("postOrder", GeneralClass.categoryId, OrderedLat, OrderedLng, AddressId, Cost, PaymentType, IsSchedule, ScheduleDate, Now, MobileNumber).done(new Action<Void>() {
                    @Override
                    public void run(Void aVoid) throws Exception {
                        Log.v(HUB_TAG, "orderReplay");
                    }
                }).onError(new ErrorCallback() {
                    @Override
                    public void onError(Throwable throwable) {
                        Log.v(HUB_TAG, "ImConnected Error");
                        //BroadcastHelper.sendInform(BroadcastHelper.HUB_CONNECTION_ERROR_BROADCAST_METHOD, context);
                    }
                });

                if (GeneralClass.hubbb == 0) {
                    Log.e(HUB_TAG, "hhhhhhhhhhhhhhhhh");
                    GeneralClass.hubbb = 1;
                    HubFactory.hub.on("OrderRejected", new SubscriptionHandler1<String>() {
                        @Override
                        public void run(String msg) {
                            Intent intent = new Intent();
                            intent.setAction("bcNewMessage");
                            intent.putExtra("id", msg);
                            intent.putExtra("type", "2");
                            mContext.sendBroadcast(intent);
                            Log.e("OrderRejected :::= ", msg);
                            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(mContext)
                                    .setSmallIcon(R.drawable.logo)
                                    .setContentTitle(context.getString(R.string.app_name))
                                    .setContentText(mContext.getString(R.string.orderRejected))
                                    .setAutoCancel(true)
                                    .setSound(defaultSoundUri)
                                    .setContentIntent(null);
                            NotificationManager notificationManager =
                                    (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
                            notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
                        }
                    }, String.class);

                    HubFactory.hub.on("OrderApproved", new SubscriptionHandler1<String>() {
                        @Override
                        public void run(String msg) {
                            Intent intent = new Intent();
                            intent.setAction("bcNewMessage");
                            intent.putExtra("id", msg);
                            intent.putExtra("type", "1");
                            mContext.sendBroadcast(intent);
                            Log.e("OrderApproved :::= ", msg);
                            Log.e("OrderApproved :::= ", "1231233");
                            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(mContext)
                                    .setSmallIcon(R.drawable.logo)
                                    .setContentTitle(context.getString(R.string.app_name))
                                    .setContentText(mContext.getString(R.string.orderaproved))
                                    .setAutoCancel(true)
                                    .setSound(defaultSoundUri)
                                    .setContentIntent(null);
                            NotificationManager notificationManager =
                                    (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
                            notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
                        }
                    }, String.class);

            /*    HubFactory.hub.on("DriverComing", new SubscriptionHandler3<String, String, String>() {
                    @Override
                    public void run(String id, String name, String mobile) {
                        Log.e("id :::= ", id);
                        Log.e("name :::= ", name);
                        Log.e("mobile :::= ", mobile);
                        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(mContext)
                                .setSmallIcon(R.mipmap.logo)
                                .setContentTitle(context.getString(R.string.app_name))
                                .setContentText(mContext.getString(R.string.Drivercoming) + mobile)
                                .setAutoCancel(true)
                                .setSound(defaultSoundUri)
                                .setContentIntent(null);
                        NotificationManager notificationManager =
                                (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
              *///          notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
                    //       }
                    //   }, String.class, String.class, String.class);


                    HubFactory.hub.on("DriverHere", new SubscriptionHandler3<String, String, String>() {
                        @Override
                        public void run(String id, String name, String mobile) {
                            Log.e("id :::= ", id);
                            Log.e("name :::= ", name);
                            Log.e("mobile :::= ", mobile);
                            Intent intent = new Intent();
                            intent.setAction("bcNewMessage");
                            intent.putExtra("type", "4");
                            intent.putExtra("id", id);
                            intent.putExtra("name",name);
                            intent.putExtra("mobile",mobile);
                            mContext.sendBroadcast(intent);
                            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(mContext)
                                    .setSmallIcon(R.drawable.logo)
                                    .setContentTitle(name)
                                    .setContentText(mContext.getString(R.string.driverhere) + mobile)
                                    .setAutoCancel(true)
                                    .setSound(defaultSoundUri)
                                    .setContentIntent(null);
                            NotificationManager notificationManager =
                                    (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
                            notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
                        }
                    }, String.class, String.class, String.class);


                    HubFactory.hub.on("MoneyConfirm", new SubscriptionHandler1<String>() {
                        @Override
                        public void run(String id) {
                            Log.e("id :::= ", id);
                            Log.e("id :::= ", "a787a");

                            Intent intent = new Intent();
                            intent.setAction("bcNewMessage");
                            intent.putExtra("type", "3");
                            intent.putExtra("id", id);
                            mContext.sendBroadcast(intent);

                            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(mContext)
                                    .setSmallIcon(R.drawable.logo)
                                    .setContentTitle(context.getString(R.string.app_name))
                                    .setContentText(mContext.getString(R.string.moneyconfirm))
                                    .setAutoCancel(true)
                                    .setSound(defaultSoundUri)
                                    .setContentIntent(null);
                            NotificationManager notificationManager =
                                    (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
                            notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());


                        }
                    }, String.class);


                    HubFactory.hub.on("NoAnswer", new SubscriptionHandler1<String>() {
                        @Override
                        public void run(String id) {
                            Log.e("id :::= ", "a7666a");
                            Intent intent = new Intent();
                            intent.setAction("bcNewMessage");
                            intent.putExtra("type", "0");
                            intent.putExtra("id", id);
                            mContext.sendBroadcast(intent);
                            Log.e("id :::= ", id);
                            Log.e("id :::= ", "a736a");
                            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(mContext)
                                    .setSmallIcon(R.drawable.logo)
                                    .setContentTitle(context.getString(R.string.app_name))
                                    .setContentText(mContext.getString(R.string.drivers))
                                    .setAutoCancel(true)
                                    .setSound(defaultSoundUri)
                                    .setContentIntent(null);
                            NotificationManager notificationManager =
                                    (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
                            notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());

                        }
                    }, String.class);


                    HubFactory.hub.on("DriverUpdateItems", new SubscriptionHandler1<String>() {
                        @Override
                        public void run(String id) {
                            Intent intent = new Intent();
                            intent.setAction("bcNewMessage");
                            intent.putExtra("type", "2406");
                            intent.putExtra("id", id);
                            mContext.sendBroadcast(intent);
                            Log.e("DriverUpdateItems id: ", id);

//                            HomeActivity.ShowUpdateItems(id);

                            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(mContext)
                                    .setSmallIcon(R.drawable.logo)
                                    .setContentTitle(context.getString(R.string.app_name))
                                    .setContentText(mContext.getString(R.string.driverupdateitems))
                                    .setAutoCancel(true)
                                    .setSound(defaultSoundUri)
                                    .setContentIntent(null);
                            NotificationManager notificationManager =
                                    (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
                            notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());


                        }
                    }, String.class);


                }




         /*       HubFactory.hub.on("OrderCanceled", new SubscriptionHandler1<String>() {
                    @Override
                    public void run(String id) {
                        Log.e("id :::= ", id);
                        Log.e("id :::= ", "a7ten");
                        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(mContext)
                                .setSmallIcon(R.mipmap.logo)
                                .setContentTitle(context.getString(R.string.app_name))
                                .setContentText(mContext.getString(R.string.ordercanceled))
                                .setAutoCancel(true)
                                .setSound(defaultSoundUri)
                                .setContentIntent(null);
                        NotificationManager notificationManager =
                                (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
                        notificationManager.notify(0 , notificationBuilder.build());
                    }
                }, String.class);
*/


            } else {
                //BroadcastHelper.sendInform(BroadcastHelper.HUB_CONNECTION_ERROR_BROADCAST_METHOD, context);
            }

        } catch (Exception e) {
            e.printStackTrace();
            //BroadcastHelper.sendInform(BroadcastHelper.HUB_CONNECTION_ERROR_BROADCAST_METHOD, context);
        }
    }


    public static void driverComing(Context context) {
        mContext = context;
        Log.e("rrrrrrrrrrrrrr", "afdafafdnuj");
        try {
            if (HubFactory.connection.getState() == ConnectionState.Connected && HubFactory.hub != null) {
                HubFactory.hub.on("driverUpdatee", new SubscriptionHandler4<String, String, String, String>() {
                    @Override
                    public void run(String id, String name, String lat, String lng) {
                        Log.e("id :::= ", id+"");
                        Log.e("lat :::= ", lat+"");
                        Log.e("lng :::= ", lng+"");
                        if (GeneralClass.trackingOpen ) {
                            Intent intent = new Intent();
                            intent.setAction("tracingorder");
                            intent.putExtra("id", id+"");
                            intent.putExtra("status", name+"");
                            intent.putExtra("lat", lat+"");
                            intent.putExtra("lng", lng+"");
                            mContext.sendBroadcast(intent);
                        }
                    }
                }, String.class, String.class, String.class, String.class);
                Log.e("Driver Updatee","out of on hub" );
            } else {
                Log.e("Driver Updatee on else","false on : HubFactory.connection.getState() == ConnectionState.Connected " +
                        "&& HubFactory.hub != null");
                //BroadcastHelper.sendInform(BroadcastHelper.HUB_CONNECTION_ERROR_BROADCAST_METHOD, context);
            }

        } catch (Exception e) {
            e.printStackTrace();
            Log.e("Driver Updatee Exp",e.getMessage());
            //BroadcastHelper.sendInform(BroadcastHelper.HUB_CONNECTION_ERROR_BROADCAST_METHOD, context);
        }
    }

    public static void itemsToLaundry(String id, final Context context) {
        Log.v("rrrrrrrrrrrrrr", "afdafafdnuj");
        try {
            if (HubFactory.connection.getState() == ConnectionState.Connected && HubFactory.hub != null) {
                Log.v("djadbjfkaabfbdbfuufb", "afdafafdnuj");
                // invoke(String.class, "iAmAvailable", "username", "password", "TransMedic")
                HubFactory.hub.invoke("itemsToLaundry", id).done(new Action<Void>() {
                    @Override
                    public void run(Void aVoid) throws Exception {
                        Log.v(HUB_TAG, "orderReplay");
                    }
                }).onError(new ErrorCallback() {
                    @Override
                    public void onError(Throwable throwable) {
                        Log.v(HUB_TAG, "ImConnected Error");
                        //BroadcastHelper.sendInform(BroadcastHelper.HUB_CONNECTION_ERROR_BROADCAST_METHOD, context);
                    }
                });
            } else {
                //BroadcastHelper.sendInform(BroadcastHelper.HUB_CONNECTION_ERROR_BROADCAST_METHOD, context);
            }

        } catch (Exception e) {
            e.printStackTrace();
            //BroadcastHelper.sendInform(BroadcastHelper.HUB_CONNECTION_ERROR_BROADCAST_METHOD, context);
        }
    }

    public static void ConfirmDriverTakeItems(String id) {
        Log.v("rrrrrrrrrrrrrr", "afdafafdnuj");
        try {
            if (HubFactory.connection.getState() == ConnectionState.Connected && HubFactory.hub != null) {
                Log.v("djadbjfkaabfbdbfuufb", "afdafafdnuj");
                // invoke(String.class, "iAmAvailable", "username", "password", "TransMedic")
                HubFactory.hub.invoke("ConfirmDriverTakeItems", id).done(new Action<Void>() {
                    @Override
                    public void run(Void aVoid) throws Exception {
                        Log.v(HUB_TAG, "ConfirmDriverTakeItems");
                    }
                }).onError(new ErrorCallback() {
                    @Override
                    public void onError(Throwable throwable) {
                        Log.v(HUB_TAG, "Error ConfirmDriverTakeItems");
                        //BroadcastHelper.sendInform(BroadcastHelper.HUB_CONNECTION_ERROR_BROADCAST_METHOD, context);
                    }
                });

            } else {
                //BroadcastHelper.sendInform(BroadcastHelper.HUB_CONNECTION_ERROR_BROADCAST_METHOD, context);
            }

        } catch (Exception e) {
            e.printStackTrace();
            //BroadcastHelper.sendInform(BroadcastHelper.HUB_CONNECTION_ERROR_BROADCAST_METHOD, context);
        }
    }

    public static void ClientFinishService(String id) {
        Log.v("rrrrrrrrrrrrrr", "afdafafdnuj");
        try {
            if (HubFactory.connection.getState() == ConnectionState.Connected && HubFactory.hub != null) {
                Log.v("djadbjfkaabfbdbfuufb", "afdafafdnuj");
                // invoke(String.class, "iAmAvailable", "username", "password", "TransMedic")
                HubFactory.hub.invoke("ClientFinishService", id).done(new Action<Void>() {
                    @Override
                    public void run(Void aVoid) throws Exception {
                        Log.v(HUB_TAG, "ClientFinishService");
                    }
                }).onError(new ErrorCallback() {
                    @Override
                    public void onError(Throwable throwable) {
                        Log.v(HUB_TAG, "Error ClientFinishService");
                        //BroadcastHelper.sendInform(BroadcastHelper.HUB_CONNECTION_ERROR_BROADCAST_METHOD, context);
                    }
                });

            } else {
                //BroadcastHelper.sendInform(BroadcastHelper.HUB_CONNECTION_ERROR_BROADCAST_METHOD, context);
            }

        } catch (Exception e) {
            e.printStackTrace();
            //BroadcastHelper.sendInform(BroadcastHelper.HUB_CONNECTION_ERROR_BROADCAST_METHOD, context);
        }
    }


    public static void cancelorder(String id) {
        Log.v("rrrrrrrrrrrrrr", "afdafafdnuj");
        try {
            if (HubFactory.connection.getState() == ConnectionState.Connected && HubFactory.hub != null) {
                Log.v("djadbjfkaabfbdbfuufb", "cancelorder");
                // invoke(String.class, "iAmAvailable", "username", "password", "TransMedic")
                HubFactory.hub.invoke("cancelorder", id).done(new Action<Void>() {
                    @Override
                    public void run(Void aVoid) throws Exception {
                        Log.v(HUB_TAG, "cancelorder");
                    }
                }).onError(new ErrorCallback() {
                    @Override
                    public void onError(Throwable throwable) {
                        Log.v(HUB_TAG, "Error cancelorder");
                        //BroadcastHelper.sendInform(BroadcastHelper.HUB_CONNECTION_ERROR_BROADCAST_METHOD, context);
                    }
                });

            } else {
                //BroadcastHelper.sendInform(BroadcastHelper.HUB_CONNECTION_ERROR_BROADCAST_METHOD, context);
            }

        } catch (Exception e) {
            e.printStackTrace();
            //BroadcastHelper.sendInform(BroadcastHelper.HUB_CONNECTION_ERROR_BROADCAST_METHOD, context);
        }
    }


    public static void driverConnected(final Context context) {
        Log.v("rrrrrrrrrrrrrr", "afdafafdnuj");
        try {
            if (HubFactory.connection.getState() == ConnectionState.Connected && HubFactory.hub != null) {
                Log.v("djadbjfkaabfbdbfuufb", "afdafafdnuj");
                // invoke(String.class, "iAmAvailable", "username", "password", "TransMedic")
                HubFactory.hub.invoke(String.class, "driverConnected").done(new Action<String>() {
                    @Override
                    public void run(String s) throws Exception {
                        Log.v(HUB_TAG, "Return String :: " + s);
                    }
                }).onError(new ErrorCallback() {
                    @Override
                    public void onError(Throwable throwable) {
                        Log.v(HUB_TAG, "Receiving Message Error");
                        //BroadcastHelper.sendInform(BroadcastHelper.HUB_CONNECTION_ERROR_BROADCAST_METHOD, context);
                    }
                });

                HubFactory.hub.on("Neworder", new SubscriptionHandler3<String, String, String>() {
                    @Override
                    public void run(String id, String name, String mobile) {
                        Log.e("id :::= ", id);
                        Log.e("name :::= ", name);
                        Log.e("mobile :::= ", mobile);
//                        Intent intent = new Intent(context, MyReceiver.class);
//                        Bundle extras = new Bundle();
//                        extras.putString("id", id);
//                        extras.putString("name", name);
//                        extras.putString("mobile", mobile);
//                        intent.putExtras(extras);
//                        context.sendBroadcast(intent);
                    }
                }, String.class, String.class, String.class);
            } else {
                //BroadcastHelper.sendInform(BroadcastHelper.HUB_CONNECTION_ERROR_BROADCAST_METHOD, context);
            }

        } catch (Exception e) {
            e.printStackTrace();
            //BroadcastHelper.sendInform(BroadcastHelper.HUB_CONNECTION_ERROR_BROADCAST_METHOD, context);
        }
    }

    public static void ConfirmDelivery(final Context context, int orderID, boolean done, String reasons) {

        if (HubFactory.connection.getState() == ConnectionState.Connected && HubFactory.hub != null) {
            HubFactory.hub.invoke("ConfirmDelivery", orderID, done, reasons).done(new Action<Void>() {
                @Override
                public void run(Void aVoid) throws Exception {
                    Log.v(HUB_TAG, "ConfirmDelivery Ok");
                }
            }).onError(new ErrorCallback() {
                @Override
                public void onError(Throwable throwable) {
                    Log.v(HUB_TAG, "ConfirmDelivery ERROR");
                }
            });
        } else {
            BroadcastHelper.sendInform(BroadcastHelper.ERROR_POST_ORDER_SEND_BROADCAST_METHOD, context);
        }
    }   // ConfirmDelivery()

  /*  public static void PostOrder(final Context context) {
        try{
            if (HubFactory.connection.getState() == ConnectionState.Connected && HubFactory.hub != null)
            {
                HubFactory.hub.invoke("ok").done(new Action<Void>() {
                    @Override
                    public void run(Void aVoid) throws Exception {
                        Log.v(HUB_TAG, "ok Ok");
                        BroadcastHelper.sendInform(BroadcastHelper.SUCCESS_POST_ORDER_SEND_BROADCAST_METHOD, context);
                    }
                }).onError(new ErrorCallback() {
                    @Override
                    public void onError(Throwable throwable) {
                        Log.v(HUB_TAG, "ok Error");
                        BroadcastHelper.sendInform(BroadcastHelper.ERROR_POST_ORDER_SEND_BROADCAST_METHOD, context);
                    }
                });

                HubFactory.hub.on( "broadcastMessage", new SubscriptionHandler1<String>() {
                    @Override
                    public void run(String msg) {
                        Log.d("result := ", msg);
                    }
                }, String.class);
            }
            else
            {
                BroadcastHelper.sendInform(BroadcastHelper.ERROR_POST_ORDER_SEND_BROADCAST_METHOD, context);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            BroadcastHelper.sendInform(BroadcastHelper.ERROR_POST_ORDER_SEND_BROADCAST_METHOD, context);

        }
    }
    */
}
