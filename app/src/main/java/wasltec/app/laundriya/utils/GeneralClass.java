package wasltec.app.laundriya.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.util.DisplayMetrics;

import wasltec.app.laundriya.models.CategoryModel;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by shafeek on 06/09/16.
 */
public class GeneralClass {
    public static String ar = "ar", en = "en";
    public static String language = "ar";
    public static int listOrGrid = 0;
    public static boolean trackingOpen =false;
    public static String name = "";
    public static String id = "";
    public static String phone = "";
    public static ArrayList<Integer> categoryId = new ArrayList<>();
    public static ArrayList<CategoryModel> category = new ArrayList<>();
    public static int hubbb = 0;


    public static void setLanguage(Context con, String lang) {

    }

    public static void checkkLanguage(Context con) {
        SharedPreferences pref = con.getApplicationContext().getSharedPreferences("language", 0);
        if (pref.getString("language", GeneralClass.en).equalsIgnoreCase(GeneralClass.en)) {
            GeneralClass.checkLanguage(GeneralClass.en, con);
        } else if (pref.getString("language", GeneralClass.ar).equalsIgnoreCase(GeneralClass.ar)) {
            GeneralClass.checkLanguage(GeneralClass.ar, con);
        }
    }

    public static void checkLanguage(String languageToLoad, Context con) {


        Locale locale = new Locale(languageToLoad);
        GeneralClass.rememberLanguage(languageToLoad, con);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        con.getApplicationContext().getResources().updateConfiguration(config, con.getApplicationContext().getResources().getDisplayMetrics());
        Resources res = con.getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        android.content.res.Configuration conf = res.getConfiguration();
        conf.setLocale(locale);
        res.updateConfiguration(conf, dm);
    }

    private static void rememberLanguage(String language, Context con) {
        GeneralClass.language = language;
        SharedPreferences pref = con.getApplicationContext().getSharedPreferences("language", 0);
        SharedPreferences.Editor ed = pref.edit();
        ed.putString("language", language);
        ed.commit();
    }


}
