package wasltec.app.laundriya.fragments;


import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.makeramen.roundedimageview.RoundedImageView;
import com.squareup.picasso.Picasso;

import wasltec.app.laundriya.R;
import wasltec.app.laundriya.activities.HomeActivity;
import wasltec.app.laundriya.activities.UpdateProfile;
import wasltec.app.laundriya.utils.LoginSharedPreferences;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyProfile extends Fragment {

    EditText name, email;
    View view;
    ImageView img;
    Button confirm;
    Context con;
    Activity act;
    LoginSharedPreferences loginSharedPreferences;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.activity_update_profile, container, false);

        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onActivityCreated(savedInstanceState);
        con = getActivity();
        act = getActivity();
        name = (EditText) getView().findViewById(R.id.username);
        email = (EditText) getView().findViewById(R.id.email);
        confirm = (Button) getView().findViewById(R.id.confirm);
        img = (ImageView) getView().findViewById(R.id.user_pp);
        loginSharedPreferences = new LoginSharedPreferences(con);

        email.setText(loginSharedPreferences.getUserMail());
        name.setText(loginSharedPreferences.getFbName());
        Picasso.with(getActivity())
                .load("https://graph.facebook.com/" + loginSharedPreferences.getFbId() + "/picture?type=large")
                .placeholder(R.drawable.logo)
                .into(img);

        //img.setImageURI(Uri.parse("https://graph.facebook.com/"+GeneralClass.id+"/picture?type=large"));

        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (email.getText().toString().trim().length() == 0) {
                    email.setError(getString(R.string.wrongmail));
                } else {
                    loginSharedPreferences.setUserMail(email.getText().toString());
                    Intent intent = new Intent(con, HomeActivity.class);
                    con.startActivity(intent);
                }

            }
        });


    }

}
