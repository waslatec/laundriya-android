package wasltec.app.laundriya.fragments;

import android.Manifest;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import wasltec.app.laundriya.DataAccess.ServerCalls;
import wasltec.app.laundriya.Dialoges.AddAddress;
import wasltec.app.laundriya.GridSpacingItemDecoration;
import wasltec.app.laundriya.activities.HomeActivity;
import wasltec.app.laundriya.R;
import wasltec.app.laundriya.adapters.CategoriesAdapter;
import wasltec.app.laundriya.models.AddressModel;
import wasltec.app.laundriya.models.AdressForLocatin;
import wasltec.app.laundriya.models.CategoryModel;
import wasltec.app.laundriya.models.StandardWebServiceResponse;
import wasltec.app.laundriya.models.SyncDataModel;
import wasltec.app.laundriya.serverconnection.volley.ConnectionVolley;
import wasltec.app.laundriya.utils.CheckResponse;
import wasltec.app.laundriya.utils.GPSTracker;
import wasltec.app.laundriya.utils.GeneralClass;
import wasltec.app.laundriya.utils.LoginSharedPreferences;
import wasltec.app.laundriya.utils.MapUtils;
import wasltec.app.laundriya.utils.Permissions;

/**
 * Created by raed on 26/04/2017.
 */

public class HomeFragment extends Fragment implements Response.ErrorListener {
    View view;
    LoginSharedPreferences loginSharedPreferences;
    MapFragment mMapFragment;
    FragmentTransaction fragmentTransaction;
    Spinner addressesSpinner;
    RecyclerView categoriesRecycler;
    Button pickupNow, pickupLater;
    ImageView search, compass;
    TextView address;
    CategoriesAdapter adapter;
    Context con;
    Activity act;
    double lat, lng;
    ArrayList<CategoryModel> categoryModelArrayList;
    String tag;
    final String GetCatTAG = "getCategories";
    CategoriesAdapter.OnItemClickListener listener;
    public static ArrayList<CategoryModel> selectedCategory;
    MapUtils maputils;
    MapUtils.OnAddressRecievedListner onAddressRecievedListner;
    ConnectionVolley addressConnectionVolley = null;
    List<AddressModel> addressModels = new ArrayList<>();
    ServerCalls calls = new ServerCalls();
    String detailsAddress, pickupbutton;
    boolean newAddress = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_home, container, false);

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        init();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onActivityCreated(savedInstanceState);


        con = getActivity();
        act = getActivity();
        loginSharedPreferences = new LoginSharedPreferences(getActivity());
        selectedCategory = new ArrayList<>();
        init();

        categoryModelArrayList = new ArrayList<>();

        tag = GetCatTAG;
        ConnectionVolley connectionVolley = null;
        calls.callServerToSyncData(con, categoryListner, this, connectionVolley);


        pickupNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddressModel address = addressModels.get(addressesSpinner.getSelectedItemPosition());
                HomeActivity.SwitchFragmentPickUpNOW(address);

            }
        });

        pickupLater.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddressModel address = addressModels.get(addressesSpinner.getSelectedItemPosition());
                HomeActivity.SwitchFragmentPickUpLater(address);

            }
        });


    }

    private void init() {
        pickupNow = (Button) getView().findViewById(R.id.pa_now);
        pickupLater = (Button) getView().findViewById(R.id.pa_later);
        compass = (ImageView) getView().findViewById(R.id.compass);
        search = (ImageView) getView().findViewById(R.id.search);
        addressesSpinner = (Spinner) getView().findViewById(R.id.address);
        categoriesRecycler = (RecyclerView) getView().findViewById(R.id.categories_recycler);
        FrameLayout fraview = (FrameLayout) getView().findViewById(R.id.map_fraview);

        addressesSpinner.setOnItemSelectedListener(AddressSpinneronItemSelectedListener);

        maputils = null;
        mMapFragment = null;
        maputils = new MapUtils();
        fraview.removeAllViews();
        mMapFragment = MapFragment.newInstance();
        fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.map_fraview, mMapFragment).commit();

        onAddressRecievedListner = new MapUtils.OnAddressRecievedListner() {
            @Override
            public void onObjectReady(AdressForLocatin addressForHome) {

//                address.setText("" + addressForHome.main);
                lat = addressForHome.lat;
                lng = addressForHome.lng;
            }
        };

        maputils.getCurrentLocation(act, con, mMapFragment, onAddressRecievedListner);


        compass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AddAddress addAddress = new AddAddress(act, lat, lng, confirmAddressListner);
                addAddress.show();
//                maputils.getCurrentLocation(act, con, mMapFragment, onAddressRecievedListner);
            }
        });


    }

    AddAddress.OnConfirmClickListener confirmAddressListner = new AddAddress.OnConfirmClickListener() {
        @Override
        public void setOnConfirmClick(double r_lat, double r_lng, String addressStr, String addressDetails) {
            calls.callServerToAddAddress(r_lat, r_lng, addressDetails, addressStr, con, addAddressListner,
                    HomeFragment.this, addressConnectionVolley);

        }
    };


    private void decodeAddressModelJson(String jsonString) {
        try {
            try {
                Gson gson = new Gson();
                StandardWebServiceResponse standardWebServiceResponse = new StandardWebServiceResponse();
                gson = new Gson();
                standardWebServiceResponse = gson.fromJson(jsonString, StandardWebServiceResponse.class);
                addressModels = new ArrayList<>();
                gson = new Gson();
                GsonBuilder builder = new GsonBuilder().excludeFieldsWithoutExposeAnnotation();
                addressModels = gson.fromJson(builder.create().toJson(standardWebServiceResponse.getAddresses()), new TypeToken<List<AddressModel>>() {
                }.getType());
                spinnerAdapter();
                if (addressConnectionVolley.dialog.isShowing()) {
                    Log.e("addressConnectionVolley", "isShowing");
                    addressConnectionVolley.dialog.dismiss();
                }
                lat = Double.parseDouble(addressModels.get(addressesSpinner.getSelectedItemPosition()).getLatitude());
                lng = Double.parseDouble(addressModels.get(addressesSpinner.getSelectedItemPosition()).getLogitude());
                maputils.setMyLocation(lat, lng);
            } catch (Exception e) {
                e.printStackTrace();
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void spinnerAdapter() {
        for (int i = 0; i < addressModels.size(); i++) {
            addressModels.get(i).setDeliveryAddress(addressModels.get(i).getDeliveryAddress().
                    replace("\n", " - ").replace("\r", " - "));
        }
        ArrayAdapter<AddressModel> spinnerAdapter = new ArrayAdapter<AddressModel>(getActivity(), R.layout.simple_item,
                addressModels);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        addressesSpinner.setAdapter(spinnerAdapter);
        spinnerAdapter.notifyDataSetChanged();
        if (newAddress) {
            addressesSpinner.setSelection((addressModels.size() - 1));
        } else {
            addressesSpinner.setSelection(getIndex2(loginSharedPreferences.getAddressId()));
            if (addressesSpinner.getSelectedItemPosition() == -1) {
                addressesSpinner.setSelection(0);
            }
        }

    }

    public int getIndex2(String itemName) {
        for (int i = 0; i < addressModels.size(); i++) {
            AddressModel auction = addressModels.get(i);
            if (itemName.equals(auction.getId())) {
                return i;
            }
        }

        return -1;
    }

    private ArrayList<CategoryModel> decodeJson(String jsonString) {
        try {
            Gson gson = new Gson();
            SyncDataModel syncDataModel = new SyncDataModel();
            gson = new Gson();
            syncDataModel = gson.fromJson(jsonString, SyncDataModel.class);
            categoryModelArrayList = new ArrayList<>();
            gson = new Gson();
            GsonBuilder builder = new GsonBuilder().excludeFieldsWithoutExposeAnnotation();
            return gson.fromJson(builder.create().toJson(syncDataModel.getCategories()),
                    new TypeToken<List<CategoryModel>>() {
                    }.getType());

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    Response.Listener addAddressListner = new Response.Listener() {

        @Override
        public void onResponse(Object response) {
            Log.e("TestingDialogDismiss", "test");

            try {
                addressConnectionVolley.dialog.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }
            Log.e("response", response.toString());
            try {
                Toast.makeText(getActivity(), getResources().getString(R.string.addressAdded), Toast.LENGTH_LONG).show();
                Intent intent = new Intent();
                intent.setAction("bcNewMessage2");
                getActivity().sendBroadcast(intent);
                newAddress = true;
                calls.callServerToGetAddress(con, addressListner, HomeFragment.this, addressConnectionVolley);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    };

    Response.Listener addressListner = new Response.Listener() {

        @Override
        public void onResponse(Object response) {
            Log.e("TestingDialogDismiss", "test");

            try {
                addressConnectionVolley.dialog.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }
            Log.e("response", response.toString());
            try {
                decodeAddressModelJson(response.toString());

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    };

    Response.Listener categoryListner = new Response.Listener() {
        @Override
        public void onResponse(Object response) {
            Log.e("response", response.toString());
            try {
                if (ConnectionVolley.dialog.isShowing()) {
                    ConnectionVolley.dialog.dismiss();
                }
//            if (tag.equals(GetCatTAG)) {
                if (CheckResponse.getInstance().checkResponse(getActivity(), response.toString(), true)) {
                    Log.e("response", "true");
                    categoryModelArrayList = decodeJson(response.toString());
                    Log.e("categorys List size", "" + categoryModelArrayList.size());

                    if (categoryModelArrayList != null) {
                        listener = new CategoriesAdapter.OnItemClickListener() {
                            @Override
                            public void onItemClick(CategoryModel item, int pos) {
                                if (item.isSelected) {
                                    selectedCategory.remove(selectedCategory.indexOf(item));
                                    categoryModelArrayList.get(pos).isSelected = false;
                                    adapter.notifyDataSetChanged();
                                } else {
                                    categoryModelArrayList.get(pos).isSelected = true;
                                    selectedCategory.add(item);
                                    adapter.notifyDataSetChanged();
                                }
                            }
                        };
                        adapter = new CategoriesAdapter(con, categoryModelArrayList, listener);
                        LinearLayoutManager daysLayoutManager = new LinearLayoutManager(con, LinearLayoutManager.HORIZONTAL, false);
                        categoriesRecycler.setLayoutManager(daysLayoutManager);
                        categoriesRecycler.setItemAnimator(new DefaultItemAnimator());
                        categoriesRecycler.addItemDecoration(new GridSpacingItemDecoration(2, GridSpacingItemDecoration.dpToPx(con, 5), true));
                        categoriesRecycler.setAdapter(adapter);
                    }
                    newAddress = false;
                    calls.callServerToGetAddress(con, addressListner, HomeFragment.this, addressConnectionVolley);
                }
//            }
            } catch (Exception e) {
                if (ConnectionVolley.dialog.isShowing()) {
                    ConnectionVolley.dialog.dismiss();

                }
//            Toast.makeText(con, con.getResources().getString(R.string.connectionError), Toast.LENGTH_LONG).show();
                Log.e("e.printStackTrace()", "" + e.getMessage());
                e.printStackTrace();
            }
        }
    };

    @Override
    public void onErrorResponse(VolleyError error) {
        VolleyLog.d("responseError", "Error: " + error.getMessage());
        try {
            ConnectionVolley.dialog.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onPause() {
//        getActivity().unregisterReceiver(this.ResponseReceiver);
        GeneralClass.trackingOpen = false;
        super.onPause();
    }


//    BroadcastReceiver ResponseReceiver = new BroadcastReceiver() {
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            // here you can update your db with new messages and update the ui (chat message list)
//            try {
//                lat = Double.parseDouble(intent.getStringExtra("lat"));
//                lng = Double.parseDouble(intent.getStringExtra("lng"));
//                mMapFragment.getMapAsync(HomeFragment.this);
//            } catch (Exception e){
//                e.printStackTrace();
//            }
//        }
//    };


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MapUtils.LOCATION_PERMISSION:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    maputils.getCurrentLocation(act, con, mMapFragment, onAddressRecievedListner);


                } else {
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    AdapterView.OnItemSelectedListener AddressSpinneronItemSelectedListener = new AdapterView.OnItemSelectedListener() {


        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            lat = Double.parseDouble(addressModels.get(position).getLatitude());
            lng = Double.parseDouble(addressModels.get(position).getLogitude());
            maputils.setMyLocation(lat, lng);

        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
        }
    };
}
