package wasltec.app.laundriya.activities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Locale;

import microsoft.aspnet.signalr.client.hubs.HubProxy;
import wasltec.app.laundriya.Dialoges.OrderDetails;
import wasltec.app.laundriya.Dialoges.OrderSuccess;
import wasltec.app.laundriya.Dialoges.OrderSuccessLater;
import wasltec.app.laundriya.Dialoges.Processing;
import wasltec.app.laundriya.R;
import wasltec.app.laundriya.adapters.MenuSimpleAdapter;
import wasltec.app.laundriya.fragments.HomeFragment;
import wasltec.app.laundriya.fragments.MyProfile;
import wasltec.app.laundriya.fragments.NavigationDrawerFragment;
import wasltec.app.laundriya.fragments.OrderHistoryDetails;
import wasltec.app.laundriya.fragments.OrderHistory;
import wasltec.app.laundriya.fragments.PickUpLater;
import wasltec.app.laundriya.fragments.PickUpNow;
import wasltec.app.laundriya.fragments.Pricing;
import wasltec.app.laundriya.fragments.Settings;
import wasltec.app.laundriya.fragments.TrackOrder;
import wasltec.app.laundriya.hub.HubCalls;
import wasltec.app.laundriya.hub.HubFactory;
import wasltec.app.laundriya.models.AddressModel;
import wasltec.app.laundriya.models.OrderModel;
import wasltec.app.laundriya.serverconnection.volley.ConnectionVolley;
import wasltec.app.laundriya.utils.GeneralClass;
import wasltec.app.laundriya.utils.LoginSharedPreferences;
import wasltec.app.laundriya.utils.Permissions;

/**
 * Created by raed on 26/04/2017.
 */

public class HomeActivity extends AppCompatActivity implements
        MenuSimpleAdapter.OnNavigationDrawerMenuItemClickedListener {
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;
    private Toolbar mtoolbar;
    static ImageView drawer, backBtn;
    static TextView txtHomeTab, subtitle;
    static Context con;
    public static HomeActivity activity;
    static FrameLayout fraview;
    static boolean on_home = true;
    LoginSharedPreferences loginSharedPreferences;
    Permissions permissions;
    static String getJson;
    static public boolean fromSettings, fromOrderHistory=false;
    static public boolean forgroun = true;
    public static Processing processing;
    Dialog nodrivers, dialogreject;
    String id = "0";
    public static boolean onlyToUpdateProfile = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_home);

        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
        fromOrderHistory=false;
        fromSettings = false;
        con = HomeActivity.this;
        activity = HomeActivity.this;
        HubCalls.clientConnected(con);
        RegisterReciver();
        initActionbar();
        forgroun = true;
        if (onlyToUpdateProfile) {
            SwitchFragmentUpdateProfile();
        } else {
            SwitchToHome();
        }
        new SetupTheHub().execute();


    }

    @Override
    protected void onRestart() {
        super.onRestart();
        con = HomeActivity.this;
        activity = HomeActivity.this;
//        HubCalls.clientConnected(con);
//        HubCalls.driverComing(con);
        initActionbar();
        forgroun = true;

//        SwitchToHome();
//        new SetupTheHub().execute();

    }

    @Override
    protected void onResume() {
        super.onResume();
        con = HomeActivity.this;
        activity = HomeActivity.this;
//        HubCalls.clientConnected(con);
//        HubCalls.driverComing(con);
        initActionbar();
        forgroun = true;
        RegisterReciver();
//        SwitchToHome();
//        new SetupTheHub().execute();
    }

    @Override
    protected void onPause() {
        super.onPause();
        forgroun = false;
    }

    @Override
    protected void onStop() {
        super.onStop();
        forgroun = false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        forgroun = false;
    }

    void init() {
        fraview = (FrameLayout) findViewById(R.id.fraview);
    }

    private static void ShowNoCategoriesDialoge() {
        final Dialog noCategories = new Dialog(con);
        if (!noCategories.isShowing()) {
            noCategories.setContentView(R.layout.nocategory_dialog);
            noCategories.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            noCategories.setCancelable(true);
            Button home = (Button) noCategories.findViewById(R.id.bkhome);

            home.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    noCategories.dismiss();


                }
            });


            noCategories.show();
        }
    }


    public static void fragmentBack() {

        HomeActivity.activity.getFragmentManager().popBackStack();
    }

    public static void ShowUpdateItems(final String id) {
        new Handler(activity.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {


                if (HomeActivity.forgroun) {
                    Log.e("OnItemUpdate", "" + id);

                    OrderDetails details = new OrderDetails(HomeActivity.activity, id);
                    details.show();

                } else {
                    Log.e("OnItemUpdate", "On background");
                }
            }
        });
//        new AlertDialog.Builder(activity)
//                .setTitle("Delete entry")
//                .setMessage("Are you sure you want to delete this entry?")
//                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int which) {
//                        // continue with delete
//                    }
//                })
//                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int which) {
//                        // do nothing
//                    }
//                })
//                .setIcon(android.R.drawable.ic_dialog_alert)
//                .show();


    }

    public static void SwitchToHome() {
        if (fromSettings) {
            Intent intent = activity.getIntent();
            activity.finish();
            activity.startActivity(intent);
        } else if (fromOrderHistory) {
            HomeActivity.fromOrderHistory=false;
            SwitchFragmentOrderHistory();

        } else {
            on_home = true;
            backBtn.setImageResource(R.drawable.home_logo);
            txtHomeTab.setText(con.getResources().getString(R.string.home_title));
            subtitle.setText(con.getResources().getString(R.string.pick_up));
            HomeFragment.selectedCategory = null;
            HomeFragment home = new HomeFragment();
            Bundle args = new Bundle();
            args.putString("json", getJson);
            home.setArguments(args);
            switchFragment(home);
        }
    }

    public static void SwitchFragmentPickUpLater(AddressModel address) {
        if (HomeFragment.selectedCategory != null && HomeFragment.selectedCategory.size() > 0) {
            on_home = false;
            backBtn.setImageResource(R.drawable.title_back);
            txtHomeTab.setText(con.getResources().getString(R.string.pa_later));
            subtitle.setText(con.getResources().getString(R.string.yourlocation));
            PickUpLater pickUp = new PickUpLater();
            Bundle args = new Bundle();
            args.putBoolean("type", false);
            args.putSerializable("addressId", address);
            pickUp.setArguments(args);
            switchFragment(pickUp);
        } else {
            HomeActivity.ShowNoCategoriesDialoge();
        }
    }

    public static void SwitchFragmentPickUpNOW(AddressModel address) {
        if (HomeFragment.selectedCategory != null && HomeFragment.selectedCategory.size() > 0) {
            on_home = false;
            backBtn.setImageResource(R.drawable.title_back);
            txtHomeTab.setText(con.getResources().getString(R.string.pa_now));
            subtitle.setText(con.getResources().getString(R.string.yourlocation));
            PickUpLater pickUp = new PickUpLater();
            Bundle args = new Bundle();
            args.putBoolean("type", true);
            args.putSerializable("addressId", address);
            pickUp.setArguments(args);
            switchFragment(pickUp);
        } else {
            HomeActivity.ShowNoCategoriesDialoge();
        }
    }

    public static void SwitchFragmentTrackOrder(String id) {

        if (id == null || id.equals("")) {
            on_home = false;
            backBtn.setImageResource(R.drawable.title_back);
            txtHomeTab.setText(con.getResources().getString(R.string.track_order_up));
            subtitle.setText("");
            OrderHistory orderHistory = new OrderHistory();

            Bundle args = new Bundle();
            args.putBoolean("toTracking", true);
            orderHistory.setArguments(args);
            switchFragment(orderHistory);
        } else {
            on_home = false;
            backBtn.setImageResource(R.drawable.title_back);
            txtHomeTab.setText(con.getResources().getString(R.string.track_order_up));
            subtitle.setText(con.getResources().getString(R.string.yourordernum));
            TrackOrder track = new TrackOrder();
            Bundle args = new Bundle();
            args.putString("id", id);
            track.setArguments(args);
            switchFragment(track);
        }
    }

    public static void SwitchFragmentOrderHistory() {
        on_home = false;
        backBtn.setImageResource(R.drawable.title_back);
        txtHomeTab.setText(con.getResources().getString(R.string.order_history_up));
        subtitle.setText("");
        OrderHistory orderHistory = new OrderHistory();

        Bundle args = new Bundle();
        args.putBoolean("toTracking", false);
        orderHistory.setArguments(args);
        switchFragment(orderHistory);

    }

    public static void SwitchFragmentSettings() {
        on_home = false;
        backBtn.setImageResource(R.drawable.title_back);
        txtHomeTab.setText(con.getResources().getString(R.string.settings));
        subtitle.setText("");

        switchFragment(new Settings());
    }

    public static void SwitchFragmentUpdateProfile() {
        on_home = false;
        backBtn.setImageResource(R.drawable.title_back);
        txtHomeTab.setText(con.getResources().getString(R.string.myprofile));
        subtitle.setText("");
        switchFragment(new MyProfile());
    }

    public static void SwitchFragmentPricing() {
        on_home = false;
        backBtn.setImageResource(R.drawable.title_back);
        txtHomeTab.setText(con.getResources().getString(R.string.pricing));
        subtitle.setText("");
        switchFragment(new Pricing());
    }

    public static void SwitchFragmentOrderHistoryDetails(OrderModel order) {
        on_home = false;
        backBtn.setImageResource(R.drawable.title_back);
        txtHomeTab.setText(con.getResources().getString(R.string.order_history));
        subtitle.setText("");
        OrderHistoryDetails details = new OrderHistoryDetails();
        Bundle args = new Bundle();
        args.putSerializable("order", order);
        details.setArguments(args);
        switchFragment(details);


    }

    static void switchFragment(Fragment newFragment) {
        FragmentTransaction ft = HomeActivity.activity.getFragmentManager()
                .beginTransaction();
        ft.replace(R.id.fraview, newFragment).commit();

//        ft.add(R.id.fraview, newFragment).addToBackStack(null)
//                .commit();
    }


    private void initActionbar() {
        mtoolbar = (Toolbar) findViewById(R.id.toolbar);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.my_drawer_layout);
        loginSharedPreferences = new LoginSharedPreferences(this);
        permissions = new Permissions();
        getJson = getIntent().getStringExtra("json");
        setSupportActionBar(mtoolbar);
        ActionBar supportActionBar = getSupportActionBar();

        if (supportActionBar != null) {
            getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
            LayoutInflater inflater;
            inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            LinearLayout layout = (LinearLayout) inflater.inflate(R.layout.home_title_bar,
                    null);
            WindowManager wm = (WindowManager) getApplicationContext().getSystemService(Context.WINDOW_SERVICE);
            Display display = wm.getDefaultDisplay();
            int width = display.getWidth(); // deprecated
            int height = display.getHeight(); // deprecated
            double y = height * 0.1;
            double x = width;
//            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
//            lp.width = x;
//            lp.copyFrom(HomeActivity.this.getWindow().getAttributes());

            android.support.v7.app.ActionBar.LayoutParams layoutParams = new ActionBar.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    (int) y);


//        ViewGroup.LayoutParams layout = holder.viewHolder.getLayoutParams();
//        layout.height = (int) 0.3 *
//            lp.width = (int) x;
//            lp.height = (int) y;
//            holder.container.setLayoutParams(new LinearLayout.LayoutParams(lp.width, lp.height));
            getSupportActionBar().setCustomView(layout, layoutParams);
//            getSupportActionBar().setCustomView(R.layout.home_title_bar);
            txtHomeTab = (TextView) findViewById(R.id.txt_home_title);
            subtitle = (TextView) findViewById(R.id.subtitle);
            backBtn = (ImageView) findViewById(R.id.img_back);
            drawer = (ImageView) findViewById(R.id.img_drawer);

            txtHomeTab.setText(getResources().getString(R.string.home_title));
            subtitle.setText(con.getResources().getString(R.string.pick_up));

            drawer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mDrawerLayout.openDrawer(Gravity.END);
                }
            });

            backBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (on_home) {
//                        HomeActivity.this.finish();
                    } else {
                        HomeActivity.SwitchToHome();
//                        HomeActivity.fragmentBack();
                    }
                }
            });


//            supportActionBar.setDisplayHomeAsUpEnabled(true);
            supportActionBar.setHomeButtonEnabled(true);

//            supportActionBar.setTitle("Home");
        }

        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                R.string.drawer_open, R.string.drawer_close) {

            /**
             * Called when a drawer has settled in a completely open state.
             */
            public void onDrawerOpened(View drawerView) {
                // getSupportActionBar().setTitle(R.string.drawer_open);
                invalidateOptionsMenu();
                mDrawerLayout.invalidate();
            }

            /**
             * Called when a drawer has settled in a completely closed state.
             */
            public void onDrawerClosed(View view) {
                // getSupportActionBar().setTitle(R.string.drawer_close);
                invalidateOptionsMenu();
                mDrawerLayout.invalidate();
            }
        };

        mDrawerToggle.setDrawerIndicatorEnabled(true);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void onNavigationMenuItemClicked(NavigationDrawerFragment.MenuItem item) {

        if (item.title.equals(con.getResources().getString(R.string.home))) {
            HomeActivity.SwitchToHome();
        } else if (item.title.equals(con.getResources().getString(R.string.track_order))) {
            HomeActivity.SwitchFragmentTrackOrder("");
        } else if (item.title.equals(con.getResources().getString(R.string.order_history))) {
            HomeActivity.SwitchFragmentOrderHistory();
        } else if (item.title.equals(con.getResources().getString(R.string.promo_cod))) {
//            HomeActivity.SwitchFragmentTrackOrder();
        } else if (item.title.equals(con.getResources().getString(R.string.pricing))) {
//            HomeActivity.SwitchFragmentTrackOrder();
            SwitchFragmentPricing();
        } else if (item.title.equals(con.getResources().getString(R.string.settings))) {

            HomeActivity.SwitchFragmentSettings();

        }

        mDrawerLayout.closeDrawer(Gravity.END);

    }


    public class SetupTheHub extends AsyncTask<Void, String, HubProxy> {
        HubFactory hubFactory;

        @Override
        protected HubProxy doInBackground(Void... params) {
            hubFactory = HubFactory.getInstance(con, loginSharedPreferences.getAccessToken(con));
            hubFactory.forceStop = false;
            return hubFactory.getTheHub();
        }

        @Override
        protected void onPostExecute(HubProxy hubProxy) {
            //  updateTitleConnection(HubFactory.connection.getState());
        }
    }   //


    public void checkkLanguage() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences("language", 0);
        if (pref.getString("language", "en").equalsIgnoreCase("en")) {
            checkLanguage("ar");
        } else if (pref.getString("language", "ar").equalsIgnoreCase("ar")) {
            checkLanguage("en");
        }
    }

    public void checkLanguage(String languageToLoad) {
        Locale locale = new Locale(languageToLoad);
        rememberLanguage(languageToLoad);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getApplicationContext().getResources().updateConfiguration(config, getApplicationContext().getResources().getDisplayMetrics());
    }

    private void rememberLanguage(String language) {
        GeneralClass.language = language;
        SharedPreferences pref = getApplicationContext().getSharedPreferences("language", 0);
        SharedPreferences.Editor ed = pref.edit();
        ed.putString("language", language);
        ed.commit();
        restartActivity();
    }

    private void restartActivity() {
        Intent intent = getIntent();
        finish();
        startActivity(intent);
    }


    private void shareApp() {
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        String shareBody = "Here is the share content body";
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, getResources().getString(R.string.app_name));
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
        startActivity(Intent.createChooser(sharingIntent, getResources().getString(R.string.shareVia)));
    }

    private void signOut() {
        loginSharedPreferences.removeLogin(this);
        finish();
        Intent toHomeActivity = new Intent(this, BoardingActivity.class);
        startActivity(toHomeActivity);
    }


    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(Gravity.END)) {
            mDrawerLayout.closeDrawer(Gravity.END);
        } else {
            if (on_home) {
                super.onBackPressed();
                HomeActivity.this.finish();
            } else {
                HomeActivity.SwitchToHome();
//                HomeActivity.fragmentBack();
            }
        }
    }


    BroadcastReceiver ResponseReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            id = intent.getStringExtra("id");
            Log.e("HubReciever", "" + intent.getStringExtra("id") + "type: " + intent.getStringExtra("type"));
            try {
                ConnectionVolley.dialog.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                if (intent.getStringExtra("type").equals("0")) {
                    processing.dismiss();
                    nodrivers();
                    Log.e("aaaaaaaa", "ffffffff");
                }

                if (intent.getStringExtra("type").equals("1")) {
                    processing.dismiss();
                    if (PickUpLater.now) {
                        OrderSuccess success = new OrderSuccess(activity, id);
                        success.show();
                    } else {
                        OrderSuccessLater success = new OrderSuccessLater(activity, id);
                        success.show();
                    }


                }
                if (intent.getStringExtra("type").equals("2")) {
                    processing.dismiss();
                    dialogreject = new Dialog(HomeActivity.activity);
                    dialogreject.setContentView(R.layout.rejected_dialog);
                    dialogreject.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    dialogreject.setCancelable(true);
                    Button bkhome = (Button) dialogreject.findViewById(R.id.homeee);

                    bkhome.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            dialogreject.dismiss();
                        }
                    });
                    dialogreject.show();

                }

                if (intent.getStringExtra("type").equals("3")) {
                    SuccessDialog(activity.getResources().getString(R.string.moneyconfirm));
                    Log.e("aaaaaaaa", "ffffffff");
                }

                if (intent.getStringExtra("type").equals("4")) {

                    String drivermessage = activity.getResources().getString(R.string.driverhere)
                            + "\n" + intent.getStringExtra("name")
                            + "\n" + intent.getStringExtra("mobile");
                    SuccessDialog(drivermessage);
                    Log.e("aaaaaaaa", "ffffffff");
                }


                if (intent.getStringExtra("type").equals("2406")) {

                    HomeActivity.ShowUpdateItems(id);
                    Log.v("ResponseReceiver 2406", "UpdateItems");
//                    callServerToGetItems(id);
//                    if (HomeActivity.forgroun) {
//                        Log.e("OnItemUpdate", "" + id);
//                        OrderDetails details = new OrderDetails(context, id);
//                        details.show();
//                    } else {
//                        Log.e("OnItemUpdate", "On background");
//                    }

                }
            } catch (Exception e) {

            }

        }
    };

    void RegisterReciver() {
        HomeActivity.activity.registerReceiver(this.ResponseReceiver, new IntentFilter("bcNewMessage"));
    }

    private void nodrivers() {
        nodrivers = new Dialog(HomeActivity.activity);
        if (!nodrivers.isShowing()) {
            nodrivers.setContentView(R.layout.nodrivers_dialog);
            nodrivers.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            nodrivers.setCancelable(true);
            Button home = (Button) nodrivers.findViewById(R.id.bkhome);

            home.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    nodrivers.dismiss();


                }
            });


            nodrivers.show();
        }

    }


    private void SuccessDialog(String message) {
        final Dialog success = new Dialog(HomeActivity.activity);
        if (!success.isShowing()) {
            success.setContentView(R.layout.nodrivers_dialog);
            success.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            success.setCancelable(true);
            Button home = (Button) success.findViewById(R.id.bkhome);
            TextView messageTxt = (TextView) success.findViewById(R.id.orderStatus);
            messageTxt.setText(message);
            home.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    success.dismiss();


                }
            });


            success.show();
        }

    }


}
