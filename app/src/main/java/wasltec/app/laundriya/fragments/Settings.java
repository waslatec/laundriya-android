package wasltec.app.laundriya.fragments;


import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.Locale;

import wasltec.app.laundriya.R;
import wasltec.app.laundriya.activities.HomeActivity;
import wasltec.app.laundriya.activities.UpdateProfile;
import wasltec.app.laundriya.utils.GeneralClass;

/**
 * Created by raed on 24/04/2017.
 */

public class Settings extends android.app.Fragment {
    Context con;
    Activity act;
    Button changeLanguage, updateProfile;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_settings, container, false);

        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onActivityCreated(savedInstanceState);
        act = getActivity();
        con = getActivity();
        changeLanguage = (Button) getView().findViewById(R.id.change_language);
        updateProfile = (Button) getView().findViewById(R.id.update_profile);

        changeLanguage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (GeneralClass.language.equals(GeneralClass.en)) {
                    GeneralClass.language = GeneralClass.ar;
                } else {
                    GeneralClass.language = GeneralClass.en;
                }
                GeneralClass.checkLanguage(GeneralClass.language, con);
                HomeActivity.fromSettings = true;
                HomeActivity.SwitchToHome();

            }
        });

        updateProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HomeActivity.SwitchFragmentUpdateProfile();

            }
        });


    }
}
