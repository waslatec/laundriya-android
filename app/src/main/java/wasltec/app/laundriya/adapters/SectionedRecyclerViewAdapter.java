package wasltec.app.laundriya.adapters;

/**
 * Created by raed on 02/05/2017.
 */

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import wasltec.app.laundriya.Dialoges.Order;
import wasltec.app.laundriya.R;
import wasltec.app.laundriya.models.CategoryModel;
import wasltec.app.laundriya.models.OrderModel;
import wasltec.app.laundriya.serverconnection.Url;
import wasltec.app.laundriya.utils.GeneralClass;
public class SectionedRecyclerViewAdapter extends RecyclerView.Adapter<SectionedRecyclerViewAdapter.SimpleViewHolder> {

    private final Context mContext;
    private List<OrderModel> mData;
    private final OnItemClickListener listener;


    public SectionedRecyclerViewAdapter(Context context, List<OrderModel> data, OnItemClickListener listener) {
        this.mContext = context;
        this.listener = listener;
        if (data != null)
            this.mData = data;
        else this.mData = new ArrayList<>();
    }

    @Override
    public int getItemViewType(int position) {
//        return mData.get(position).isHeader ? 1 : 2;
        return 2;
    }

    public void add(OrderModel s, int position) {
        position = position == -1 ? getItemCount() : position;
        mData.add(position, s);
        notifyItemInserted(position);
    }


    public void remove(int position) {
        if (position < getItemCount()) {
            mData.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void refresh() {
        notifyDataSetChanged();
    }

    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view;
//        if (viewType == 1)
//            view = LayoutInflater.from(mContext).inflate(R.layout.menu_section, parent, false);
//        else
        view = LayoutInflater.from(mContext).inflate(R.layout.section_ex1_item, parent, false);

        return new SimpleViewHolder(view, viewType);
    }

    @Override
    public void onBindViewHolder(SimpleViewHolder holder, final int position) {

        final OrderModel item = mData.get(position);

        if (GeneralClass.language.equals("en")) {
            holder.orders.setText(item.getCategories().toString().replace("[","").replace("]",""));
        } else if (GeneralClass.language.equals("ar")) {
            holder.orders.setText(item.getCategories().toString().replace("[","").replace("]",""));
        }

        holder.id.setText(item.getId()+"");
        holder.price.setText(item.getCost() +mContext.getResources().getString(R.string.sar));
        holder.Date.setText(item.getDate().replace("T"," "));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(item,position);

            }
        });

    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public class SimpleViewHolder extends RecyclerView.ViewHolder {
        public final TextView orders;
        public final TextView price;
        public final TextView Date;
        public final TextView id;


        public SimpleViewHolder(View view, int viewType) {
            super(view);
            orders = (TextView) view.findViewById(R.id.orders);
            price = (TextView) view.findViewById(R.id.price);
            Date = (TextView) view.findViewById(R.id.date);
            id = (TextView) view.findViewById(R.id.orderid);

//            if (viewType == 2)
//                view.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        int pos = getLayoutPosition();
//                        setSelected(pos);
//                        container.setBackgroundResource(R.drawable.item_bg);
//                        txtCategory.setTextColor(mContext.getResources().getColor(R.color.lightBlue));
////                        imgCategory.setImageResource(R.drawable.);
//
//                        notifyDataSetChanged();
//                    }
//
//                });
        }
    }



    public interface OnItemClickListener {
        void onItemClick(OrderModel item, int pos);
    }
}

