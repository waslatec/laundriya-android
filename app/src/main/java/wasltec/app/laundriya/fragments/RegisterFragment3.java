package wasltec.app.laundriya.fragments;


import android.app.Activity;
import android.content.Context;
import android.nfc.TagLostException;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.dpizarro.pinview.library.PinView;

import wasltec.app.laundriya.DataAccess.ServerCalls;
import wasltec.app.laundriya.R;
import wasltec.app.laundriya.activities.Register;
import wasltec.app.laundriya.serverconnection.volley.ConnectionVolley;
import wasltec.app.laundriya.utils.CheckResponse;
import wasltec.app.laundriya.utils.GeneralClass;

/**
 * Created by raed on 24/04/2017.
 */

public class RegisterFragment3 extends Fragment implements Response.Listener, Response.ErrorListener {
    final String TAGsignIn = "SIGNIN", TAGconfirm = "CONFIRM";
    String tag = "";
    Context con;
    Activity act;
    PinView pinView;
    ServerCalls calls = new ServerCalls();
    String code = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_reg3, container, false);

        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onActivityCreated(savedInstanceState);
        act = getActivity();
        con = getActivity();
        pinView = (PinView) getView().findViewById(R.id.pinView);
        pinView.setPin(6);
        pinView.setTextSizePinBoxes(act.getResources().getDimension(R.dimen._14sdp));
        pinView.setOnCompleteListener(new PinView.OnCompleteListener() {
            @Override
            public void onComplete(boolean completed, final String pinResults) {
                //Do what you want
                if (completed) {
                    code = pinResults;
                    tag = TAGconfirm;
                    ConnectionVolley ConnectionVolley=null;
                    calls.callServerToConfirm(code, Register.user.getMobilenumber(), con, RegisterFragment3.this,
                            RegisterFragment3.this,ConnectionVolley);


                }
            }
        });
    }


    //381415
    @Override
    public void onResponse(Object response) {
        Log.e("response tag:"+tag, response.toString());
        try {
            if (CheckResponse.getInstance().checkResponse(con, response.toString(), true)) {
                if (tag.equals(TAGconfirm)) {
                    ConnectionVolley ConnectionVolley=null;
                    calls.callServerToConfirm(code, Register.user.getMobilenumber(), con, RegisterFragment3.this,
                            RegisterFragment3.this,ConnectionVolley);
                    tag = TAGsignIn;
                } else if (tag.equals(TAGsignIn)) {

                    calls.rememberUser(response.toString(), con);
                    GeneralClass.phone=Register.user.getMobilenumber();
                    Register.NextTab();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        VolleyLog.d("responseError", "Error: " + error.getMessage());
        try {
            ConnectionVolley.dialog.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
