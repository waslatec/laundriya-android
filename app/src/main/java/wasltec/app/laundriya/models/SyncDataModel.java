package wasltec.app.laundriya.models;

/**
 * Created by ahmed on 2/14/17.
 */

public class SyncDataModel {

    private String Success;
    private String Code;
    private String EnglishMessage;
    private String ItemImgURL;
    private String CategoryImgURL;
    private String ArabicMessage;
    private Object categories;
    private Object Items;
    private Object services;
    private Object servicestypes;
    private Object prices;
    private String buldings;
    private String ServiceName;
    private String MaxDate;

    public String getSuccess() {
        return Success;
    }

    public void setSuccess(String success) {
        Success = success;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String code) {
        Code = code;
    }

    public String getEnglishMessage() {
        return EnglishMessage;
    }

    public void setEnglishMessage(String englishMessage) {
        EnglishMessage = englishMessage;
    }

    public String getItemImgURL() {
        return ItemImgURL;
    }

    public void setItemImgURL(String itemImgURL) {
        ItemImgURL = itemImgURL;
    }

    public String getCategoryImgURL() {
        return CategoryImgURL;
    }

    public void setCategoryImgURL(String categoryImgURL) {
        CategoryImgURL = categoryImgURL;
    }

    public String getArabicMessage() {
        return ArabicMessage;
    }

    public void setArabicMessage(String arabicMessage) {
        ArabicMessage = arabicMessage;
    }

    public Object getCategories() {
        return categories;
    }

    public void setCategories(Object categories) {
        this.categories = categories;
    }

    public Object getItems() {
        return Items;
    }

    public void setItems(Object items) {
        Items = items;
    }

    public Object getServices() {
        return services;
    }

    public void setServices(Object services) {
        this.services = services;
    }

    public Object getServicestypes() {
        return servicestypes;
    }

    public void setServicestypes(Object servicestypes) {
        this.servicestypes = servicestypes;
    }

    public Object getPrices() {
        return prices;
    }

    public void setPrices(Object prices) {
        this.prices = prices;
    }

    public String getBuldings() {
        return buldings;
    }

    public void setBuldings(String buldings) {
        this.buldings = buldings;
    }

    public String getServiceName() {
        return ServiceName;
    }

    public void setServiceName(String serviceName) {
        ServiceName = serviceName;
    }

    public String getMaxDate() {
        return MaxDate;
    }

    public void setMaxDate(String maxDate) {
        MaxDate = maxDate;
    }
}
