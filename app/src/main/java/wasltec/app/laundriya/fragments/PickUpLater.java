package wasltec.app.laundriya.fragments;


import android.app.Activity;
import android.app.Dialog;
import android.app.FragmentTransaction;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.google.android.gms.maps.MapFragment;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import wasltec.app.laundriya.DataAccess.ServerCalls;
import wasltec.app.laundriya.Dialoges.Order;
import wasltec.app.laundriya.Dialoges.Order.OnOrderAcceptClickListener;
import wasltec.app.laundriya.Dialoges.OrderDetails;
import wasltec.app.laundriya.Dialoges.OrderSuccess;
import wasltec.app.laundriya.Dialoges.OrderSuccessLater;
import wasltec.app.laundriya.Dialoges.Processing;
import wasltec.app.laundriya.GridSpacingItemDecoration;
import wasltec.app.laundriya.activities.HomeActivity;
import wasltec.app.laundriya.hub.HubCalls;
import wasltec.app.laundriya.models.AddressModel;
import wasltec.app.laundriya.models.AdressForLocatin;
import wasltec.app.laundriya.models.DeliveryDate;
import wasltec.app.laundriya.models.DeliveryTime;
import wasltec.app.laundriya.R;
import wasltec.app.laundriya.adapters.DaysAdapter;
import wasltec.app.laundriya.adapters.TimesAdapter;
import wasltec.app.laundriya.models.OrderModel;
import wasltec.app.laundriya.models.StandardWebServiceResponse;
import wasltec.app.laundriya.models.off_days_model;
import wasltec.app.laundriya.models.times_model;
import wasltec.app.laundriya.serverconnection.Url;
import wasltec.app.laundriya.serverconnection.volley.AppController;
import wasltec.app.laundriya.serverconnection.volley.ConnectionVolley;
import wasltec.app.laundriya.utils.GeneralClass;
import wasltec.app.laundriya.utils.LoginSharedPreferences;
import wasltec.app.laundriya.utils.MapUtils;

/**
 * Created by raed on 24/04/2017.
 */

public class PickUpLater extends android.app.Fragment implements Response.ErrorListener {
    Context con;
    Activity act;
    RecyclerView daysList, timesList;
    TextView address;
    DaysAdapter adapter;
    TimesAdapter timesAdapter;
    RelativeLayout notes;
    Button confirm;
    TextView date, day, houres;
    LinearLayout dateContainer;
    ServerCalls calls = new ServerCalls();
    public final int TAGGetAddress = 0;
    public final int TAGGetitems = 1;
    public final int TAGGetdate = 2;
    public final int TAGGettime = 3;
    static int requestType;
    AddressModel addressModel;
    ArrayList<off_days_model> days_models;
    ArrayList<times_model> timesModel;
    ArrayList<Date> dateArrayList = new ArrayList<>();
    Date today;
    Date selectedDate;
    String selectedTime;
    double lat, lng;
    LoginSharedPreferences loginSharedPreferences;
    OnOrderAcceptClickListener onOrderAcceptClickListener;
    String phone = "";

    String id = "0";
    MapUtils maputils;
    MapUtils.OnAddressRecievedListner onAddressRecievedListner;
    View view;
    MapFragment mMapFragment;
    FragmentTransaction fragmentTransaction;
    TimesAdapter.OnItemClickedListener timesListener;
    DaysAdapter.OnItemClickedListener daysListener;
    ConnectionVolley addressConnectionVolley = null, datesConnectionVolley = null, timessConnectionVolley = null;
    public static boolean now = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_pick_up_later, container, false);

        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onActivityCreated(savedInstanceState);

        now = getArguments().getBoolean("type");
        addressModel = (AddressModel) getArguments().getSerializable("addressId");
        lat=Double.parseDouble(addressModel.getLatitude());
        lng=Double.parseDouble(addressModel.getLogitude());
        act = getActivity();
        con = getActivity();
        loginSharedPreferences = new LoginSharedPreferences(getActivity());
//        RegisterReciver();
        daysList = (RecyclerView) getView().findViewById(R.id.days_list);
        timesList = (RecyclerView) getView().findViewById(R.id.times_list);
        notes = (RelativeLayout) getView().findViewById(R.id.notes);
        confirm = (Button) getView().findViewById(R.id.confirm);
        address = (TextView) getView().findViewById(R.id.subtitle);
        day = (TextView) getView().findViewById(R.id.txt_day);
        date = (TextView) getView().findViewById(R.id.txt_date);
        houres = (TextView) getView().findViewById(R.id.txt_houre);
        dateContainer = (LinearLayout) getView().findViewById(R.id.date_container);

        if (now) {
            dateContainer.setVisibility(View.GONE);
        } else {
            dateContainer.setVisibility(View.VISIBLE);
        }
        phone = loginSharedPreferences.getPhoneNumber(act);

        mMapFragment = MapFragment.newInstance();
        fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.mfraview, mMapFragment);
        fragmentTransaction.commit();

        maputils = new MapUtils();

        onAddressRecievedListner = new MapUtils.OnAddressRecievedListner() {
            @Override
            public void onObjectReady(AdressForLocatin addressForHome) {


            }
        };
        address.setText(addressModel.getDeliveryAddress());

        maputils.getCurrentLocation(act, con, mMapFragment, onAddressRecievedListner);
        try {

            maputils.setMyLocation(Double.parseDouble(addressModel.getLatitude()), Double.parseDouble(addressModel.getLogitude()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (!now) {
            calls.callServerToGetdate(con, datesListner, PickUpLater.this, datesConnectionVolley);
        }
        requestType = TAGGetdate;


        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Log.e("selectedCategory", HomeFragment.selectedCategory.size() + "");
                    Order order = new Order(act, HomeFragment.selectedCategory, onOrderAcceptClickListener);
                    order.show();
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }
        });

        onOrderAcceptClickListener = new OnOrderAcceptClickListener() {
            @Override
            public void setOnAcceptClick() {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
                if (now) {


                    Log.e("Phone", phone);
                    String currentDateandTime = sdf.format(new Date());
                    HubCalls.postOrder(getActivity(), lat + "", lng + "",
                            addressModel.getId(), "0.0",
                            "2", false, currentDateandTime, true, phone);


                    loginSharedPreferences.setAddressId(addressModel.getId());

                } else {

                    String date = sdf.format(selectedDate);
                    HubCalls.postOrder(getActivity(), lat + "", lng + "",
                            addressModel.getId(),
                            "0.0", "2", true, date + " " + selectedTime + ".000", true, phone);

                }


                HomeActivity.processing = new Processing(HomeActivity.activity);
                HomeActivity.processing.show();

            }
        };


    }


    private off_days_model GetModelByDate(Date date) {
        off_days_model events_model = null;
        DateFormat formatter;

        formatter = new SimpleDateFormat("dd/MM/yyyy");
        for (int i = 0; i < days_models.size(); i++) {
            Date evdate = null;
            try {
                evdate = (Date) formatter.parse(String.valueOf(days_models.get(i).getFromDate()));
                evdate = removeTime(evdate);
                Log.d("selected", date.toString());
                Log.d("selected2", evdate.toString());

                if (evdate.compareTo(date) == 0) {
                    events_model = days_models.get(i);
                    return events_model;
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }

        }

        return events_model;
    }


    public static Date removeTime(Date date) {
        java.util.Calendar cal = java.util.Calendar.getInstance();
        cal.setTime(date);
        cal.set(java.util.Calendar.HOUR_OF_DAY, 0);
        cal.set(java.util.Calendar.MINUTE, 0);
        cal.set(java.util.Calendar.SECOND, 0);
        cal.set(java.util.Calendar.MILLISECOND, 0);
        return cal.getTime();
    }


    private void decodeGetItemsJson(String jsonString) {
//        try {
//            Gson gson = new Gson();
//            StandardWebServiceResponse standardWebServiceResponse = gson.fromJson(jsonString, StandardWebServiceResponse.class);
//            orderModelArrayList = new ArrayList<>();
//            gson = new Gson();
//            GsonBuilder builder = new GsonBuilder().excludeFieldsWithoutExposeAnnotation();
//            orderModelArrayList = gson.fromJson(builder.create().toJson(standardWebServiceResponse.getOrdersList()), new TypeToken<List<OrderModel>>(){}.getType());
//
//            gson = new Gson();
//            builder = new GsonBuilder().excludeFieldsWithoutExposeAnnotation();
//            itemsModelArrayList = gson.fromJson(builder.create().toJson(orderModelArrayList.get(getIndex(id)).getItems()), new TypeToken<List<ItemsModel>>(){}.getType());
//
//
//            final Dialog dialog2 = new Dialog(getActivity());
//            dialog2.setContentView(R.layout.items_dialog2);
//            dialog2.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//            Button confirm = (Button)dialog2.findViewById(R.id.withdriver);
//            TextView cost = (TextView)dialog2.findViewById(R.id.cost);
//            TextView orderID = (TextView)dialog2.findViewById(R.id.i);
//            RecyclerView recyclerView = (RecyclerView) dialog2.findViewById(R.id.recyclerView);
//            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
//
//
//            sponsor_adapter x = new sponsor_adapter(getActivity(), itemsModelArrayList);
//            recyclerView.setAdapter(x);
//
//            cost.setText(getActivity().getString(R.string.total) +" "+ orderModelArrayList.get(getIndex(id)).getCost());
//            orderID.setText(getActivity().getString(R.string.orderid) +" "+ orderModelArrayList.get(getIndex(id)).getId());
//
//            confirm.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//
//                    dialog2.dismiss();
//                    HubCalls.ConfirmDriverTakeItems(id);
//
//                }
//            });
//            dialog2.show();
//
//        } catch (Exception e){
//            e.printStackTrace();
//        }
    }

    private void decodeGetDatesJson(String jsonString) {
        try {
            Gson gson = new Gson();
            StandardWebServiceResponse standardWebServiceResponse = new StandardWebServiceResponse();
            gson = new Gson();
            standardWebServiceResponse = gson.fromJson(jsonString, StandardWebServiceResponse.class);
            days_models = new ArrayList<>();
            GsonBuilder builder = new GsonBuilder().excludeFieldsWithoutExposeAnnotation();
            days_models = gson.fromJson(builder.create().toJson(standardWebServiceResponse.getWorkHouresData()), new TypeToken<List<off_days_model>>() {
            }.getType());

            java.util.Calendar nextYear = java.util.Calendar.getInstance();
            nextYear.add(java.util.Calendar.YEAR, 1);
            today = removeTime(new Date());
            ArrayList<Date> selectedDates = new ArrayList<>();
            DateFormat formatter;


            formatter = new SimpleDateFormat("yyyy/MM/dd");

            for (int i = 0; i < days_models.size(); i++) {
                try {
                    //Date date = (Date) formatter.parse(days_models.get(i).getToDate());
                    //date = removeTime(date);
                    //selectedDates.add(date);

                    DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);

                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(format.parse(days_models.get(i).getFromDate()));

                    while (calendar.getTime().before(format.parse(days_models.get(i).getToDate()))) {
                        Date result = calendar.getTime();
                        dateArrayList.add(result);
                        calendar.add(Calendar.DATE, 1);
                    }
                    dateArrayList.add(format.parse(days_models.get(i).getToDate()));

                    //dateArrayList.add(format.parse(days_models.get(i).getFromDate()));

                } catch (ParseException e) {
                    e.printStackTrace();
                }


            }
//            Toast.makeText(getActivity(), dateArrayList.get(0).toString(), Toast.LENGTH_SHORT).show();


            //implement recycler here
            if (dateArrayList != null && dateArrayList.size() > 0) {
                if (selectedDate == null) {
                    selectedDate = dateArrayList.get(0);
                }
            }

            timesListener = new TimesAdapter.OnItemClickedListener() {
                @Override
                public void setOnItemClicked(String item) {
                    selectedTime = item;
                    houres.setText("" + selectedTime);
                }
            };

            daysListener = new DaysAdapter.OnItemClickedListener() {
                @Override
                public void setOnItemClicked(Date item, int pos) {
                    adapter.isSelected = pos;
                    DaySelected(item);
                    adapter.notifyDataSetChanged();


                }
            };

            adapter = new DaysAdapter(con, dateArrayList, daysListener);

            LinearLayoutManager daysLayoutManager = new LinearLayoutManager(con, LinearLayoutManager.HORIZONTAL, false);
            daysList.setLayoutManager(daysLayoutManager);
            daysList.setItemAnimator(new DefaultItemAnimator());
//            daysList.addItemDecoration(new GridSpacingItemDecoration(2, GridSpacingItemDecoration.dpToPx(con, 5), true));

            daysList.setAdapter(adapter);
            if (dateArrayList != null && dateArrayList.size() > 0) {
                DaySelected(dateArrayList.get(0));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void DaySelected(Date r_date) {
        selectedDate = r_date;
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(selectedDate);
        String dateName = calendar.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.SHORT, Locale.US);
        day.setText("" + dateName);
        date.setText("" + selectedDate.getDay());
        calls.callServerToGettime(con, timesListner, PickUpLater.this, timessConnectionVolley);

    }

    private void decodeGetTimeJson(String jsonString) {
        try {
            Gson gson = new Gson();
            StandardWebServiceResponse standardWebServiceResponse = new StandardWebServiceResponse();
            gson = new Gson();
            standardWebServiceResponse = gson.fromJson(jsonString, StandardWebServiceResponse.class);
            timesModel = new ArrayList<>();
            gson = new Gson();
            GsonBuilder builder = new GsonBuilder().excludeFieldsWithoutExposeAnnotation();
            timesModel = gson.fromJson(builder.create().toJson(standardWebServiceResponse.getData()),
                    new TypeToken<List<times_model>>() {

                    }.getType());
            //implement times recycler here
            Calendar c = Calendar.getInstance();
            c.setTime(selectedDate);
            int dayOfWeek = c.get(Calendar.DAY_OF_WEEK) - 1;
            for (int i = 0; i < timesModel.size(); i++) {
                if (timesModel.get(i).getDay().equals(dayOfWeek + "")) {
                    if (timesModel.get(i).getTimes() != null && timesModel.get(i).getTimes().size() > 0) {
                        if (selectedTime == null) {
                            selectedTime = timesModel.get(i).getTimes().get(0);
                            houres.setText("" + selectedTime);
                        }
                    }
                    Log.e("Selected Date", "" + selectedDate + selectedTime);
                    timesAdapter = new TimesAdapter(con, timesModel.get(i).getTimes(), timesListener);
                    LinearLayoutManager timesLayoutManager = new LinearLayoutManager(con, LinearLayoutManager.VERTICAL, false);
                    timesList.setLayoutManager(timesLayoutManager);
//                RecyclerView.LayoutManager mLayoutManager = new  GridLayoutManager(MainActivity.this, 2);
//                recyclerView.setLayoutManager(mLayoutManager);
//                recyclerView.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(10), true));
                    timesList.setItemAnimator(new DefaultItemAnimator());
                    timesList.setAdapter(timesAdapter);
                    return;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private boolean getRange(Date min, Date max, Date current) {
        return current.after(min) && current.before(max);
    }

    //    public int getIndex(String itemName) {
//        for (int i = 0; i < orderModelArrayList.size(); i++) {
//            OrderModel auction = orderModelArrayList.get(i);
//            if (itemName.equals(auction.getId())) {
//                return i;
//            }
//        }
//
//        return -1;
//    }



    Response.Listener datesListner = new Response.Listener() {

        @Override
        public void onResponse(Object response) {
            try {
                datesConnectionVolley.dialog.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }
            Log.e("response", response.toString());
            try {
                decodeGetDatesJson(response.toString());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    Response.Listener timesListner = new Response.Listener() {

        @Override
        public void onResponse(Object response) {
            try {
                timessConnectionVolley.dialog.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }
            Log.e("response", response.toString());
            try {
                decodeGetTimeJson(response.toString());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

//    @Override
//    public void onResponse(Object response) {
//        try {
//            ConnectionVolley.dialog.dismiss();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        Log.e("response", response.toString());
//        try {
//
//            if (requestType == TAGGetAddress) {
//                decodeAddressModelJson(response.toString());
//            } else if (requestType == TAGGetitems) {
//                decodeGetItemsJson(response.toString());
//            } else if (requestType == TAGGetdate) {
//                decodeGetDatesJson(response.toString());
//                calls.callServerToGettime(con, PickUpLater.this, PickUpLater.this);
//                requestType = TAGGettime;
//            } else if (requestType == TAGGettime) {
//                decodeGetTimeJson(response.toString());
//            }
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }

    @Override
    public void onErrorResponse(VolleyError error) {
        VolleyLog.d("responseError", "Error: " + error.getMessage());
        try {
            ConnectionVolley.dialog.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onResume() {
//        calls.callServerToGetAddress(con, addressListner, PickUpLater.this);
        super.onResume();
    }

    @Override
    public void onPause() {
//        getActivity().unregisterReceiver(this.ResponseReceiver);


        super.onPause();
    }



}
