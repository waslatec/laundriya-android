package wasltec.app.laundriya.Dialoges;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.skyfishjy.library.RippleBackground;

import wasltec.app.laundriya.R;

/**
 * Created by raed on 02/05/2017.
 */

public class Processing extends Dialog {

    public Activity c;
    public Dialog d;
    public ImageView img_cancel;
    public LinearLayout lin_cancel;
    public TextView txt_cancel;

    public Processing(Activity a) {
        super(a);
        // TODO Auto-generated constructor stub
        this.c = a;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().setBackgroundDrawable(new ColorDrawable(c.getResources().getColor(R.color.tran_blue)));
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_proccessing);
        WindowManager wm = (WindowManager) c.getApplicationContext().getSystemService(c.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        int width = display.getWidth(); // deprecated
        int height = display.getHeight(); // deprecated
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(c.getWindow().getAttributes());
        lp.width = width;
        lp.height = height;
//        lp.type = WindowManager.LayoutParams.TYPE_SYSTEM_ERROR;

        if(lp.width>0&&lp.height>0){
            getWindow().setAttributes((new WindowManager.LayoutParams(lp.width, lp.height)));
        }else{

            height=(int)c.getResources().getDimension(R.dimen._480sdp);
            width=(int)c.getResources().getDimension(R.dimen._320sdp);

            getWindow().setAttributes((new WindowManager.LayoutParams(width, height)));
        }

        lin_cancel = (LinearLayout) findViewById(R.id.lin_cancel);
        img_cancel = (ImageView) findViewById(R.id.img_cancel);
        txt_cancel = (TextView) findViewById(R.id.txt_cancel);

        lin_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Processing.this.dismiss();
            }
        });

        final RippleBackground rippleBackground = (RippleBackground) findViewById(R.id.content);
        ImageView imageView = (ImageView) findViewById(R.id.centerImage);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });
        rippleBackground.startRippleAnimation();


    }


}
