package wasltec.app.laundriya.activities;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;

import io.fabric.sdk.android.Fabric;
import wasltec.app.laundriya.CustomeViews.NonSwipeableViewPager;

import wasltec.app.laundriya.R;
import wasltec.app.laundriya.adapters.RegisterPagerAdapter;
import wasltec.app.laundriya.models.UserModel;
import wasltec.app.laundriya.serverconnection.volley.AppController;
import wasltec.app.laundriya.utils.GeneralClass;

/**
 * Created by raed on 24/04/2017.
 */

public class Register extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {
    static NonSwipeableViewPager register_v_p;
    TabLayout tabLayout;
    RegisterPagerAdapter mFragmentAdapter;
    final static int lastItemnum = 2;
    public static UserModel user = new UserModel();
    static Context con;
    static Activity activity;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
        TwitterAuthConfig authConfig = new TwitterAuthConfig(AppController.TWITTER_KEY, AppController.TWITTER_SECRET);
        Fabric.with(this, new Twitter(authConfig));

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_reg);
//        getSupportActionBar().hide();
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        con = Register.this;
        activity = Register.this;
        init();
        initOnClieckListner();
        mFragmentAdapter = new RegisterPagerAdapter(getSupportFragmentManager());
        register_v_p.setAdapter(mFragmentAdapter);


    }

    void init() {

        register_v_p = (NonSwipeableViewPager) findViewById(R.id.register_viewpager);
        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.setupWithViewPager(register_v_p, true);

    }

    public static void NextTab() {
        int current = register_v_p.getCurrentItem();
        if (current < lastItemnum) {
            current++;
            register_v_p.setCurrentItem(current);
//                    btnPrev.setBackgroundResource(R.drawable.);
            if ((current) == lastItemnum) {


            }

        } else {
//            UserModel user = new UserModel();
            Intent intent = new Intent(con, HomeActivity.class);
//                    intent.putExtra("",user);
//                    UserModel userModel = (UserModel) getIntent().getExtras().getSerializable("");
            activity.startActivity(intent);

        }
    }

    public static void previouseTab() {

        int current = register_v_p.getCurrentItem();
        if (current > 0) {
            current--;
            register_v_p.setCurrentItem(current);
            if ((current) < lastItemnum) {
            }
            if ((current) == 0) {

            }

        } else {
            activity.finish();
        }

    }

    void initOnClieckListner() {


    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d("GOOGLE", "onConnectionFailed:" + connectionResult);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK)
            return;
        if (null == data)
            return;
        Uri originalUri = null;
        Log.v("CallBack From Login", "We are in");
        Fragment fragment = getFragmentManager().findFragmentById(R.id.register_viewpager);
        if (fragment != null) {
            fragment.onActivityResult(requestCode, resultCode, data);

        }
        super.onActivityResult(requestCode, resultCode, data);

    }

}
