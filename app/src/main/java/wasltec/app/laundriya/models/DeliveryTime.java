package wasltec.app.laundriya.models;

/**
 * Created by raed on 02/05/2017.
 */

public class DeliveryTime {

    String from, to, state;
    public boolean isSelected;

    public DeliveryTime() {
    }

    public DeliveryTime(String from, String to, String state) {

        this.from = from;
        this.to = to;
        this.state = state;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getFrom() {
        return from;
    }

    public String getTo() {
        return to;
    }

    public String getState() {
        return state;
    }
}
