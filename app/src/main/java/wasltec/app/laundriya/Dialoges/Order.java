package wasltec.app.laundriya.Dialoges;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import wasltec.app.laundriya.R;
import wasltec.app.laundriya.activities.HomeActivity;
import wasltec.app.laundriya.fragments.HomeFragment;
import wasltec.app.laundriya.models.CategoryModel;
import wasltec.app.laundriya.utils.GeneralClass;

/**
 * Created by raed on 02/05/2017.
 */

public class Order extends Dialog {

    public Activity c;
    public Dialog d;
    public ImageView img_accept, img_cancel;
    public LinearLayout lin_accept, lin_cancel;
    public TextView txt_accept, txt_cancel;
    ListView ordersList;
    List<CategoryModel> categories;
    List<String> orders = new ArrayList<>();
    OnOrderAcceptClickListener listner;

    public Order(Activity a, List<CategoryModel> categories, OnOrderAcceptClickListener listner) {
        super(a);
        // TODO Auto-generated constructor stub
        this.c = a;
        this.categories = categories;
        this.listner = listner;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().setBackgroundDrawable(new ColorDrawable(c.getResources().getColor(R.color.tran_blue)));
//        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_order);

//        WindowManager wm = (WindowManager) c.getApplicationContext().getSystemService(c.WINDOW_SERVICE);
//        Display display = wm.getDefaultDisplay();
//        int width = display.getWidth(); // deprecated
//        int height = display.getHeight(); // deprecated
//        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
//        lp.copyFrom(c.getWindow().getAttributes());
//        lp.width = width;
//        lp.height = height;
//        getWindow().setAttributes((new WindowManager.LayoutParams(lp.width, lp.height)));

        lin_accept = (LinearLayout) findViewById(R.id.lin_accept);
        lin_cancel = (LinearLayout) findViewById(R.id.lin_cancel);
        img_accept = (ImageView) findViewById(R.id.img_accept);
        img_cancel = (ImageView) findViewById(R.id.img_cancel);
        txt_accept = (TextView) findViewById(R.id.txt_accept);
        txt_cancel = (TextView) findViewById(R.id.txt_cancel);
        ordersList = (ListView) findViewById(R.id.orders_list);
//        LinearLayout container = (LinearLayout)findViewById((R.id.container));
//        container.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
//                ViewGroup.LayoutParams.MATCH_PARENT));
        lin_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Order.this.dismiss();
            }
        });
        GeneralClass.categoryId = new ArrayList<>();
        for (int i = 0; i < HomeFragment.selectedCategory.size(); i++) {
            if (GeneralClass.language.equals("en")) {
                orders.add(HomeFragment.selectedCategory.get(i).getEnglishName() + " : " + HomeFragment.selectedCategory.get(i).getmin());
            } else if (GeneralClass.language.equals("ar")) {
                orders.add(HomeFragment.selectedCategory.get(i).getArabicName() + " : " + HomeFragment.selectedCategory.get(i).getmin());
            }
            GeneralClass.categoryId.add(Integer.parseInt(HomeFragment.selectedCategory.get(i).getId()));
        }
        String[] ordersarray = orders.toArray(new String[orders.size()]);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(c,
                R.layout.simple_item_blue, ordersarray);
        ordersList.setAdapter(adapter);

        lin_accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Order.this.dismiss();

                listner.setOnAcceptClick();


            }
        });


    }


    public interface OnOrderAcceptClickListener {
        void setOnAcceptClick();
    }

}
